/*
 Navicat Premium Data Transfer

 Source Server         : 10.15.15.56
 Source Server Type    : MySQL
 Source Server Version : 50738
 Source Host           : 10.15.15.56:3306
 Source Schema         : ppp_server

 Target Server Type    : MySQL
 Target Server Version : 50738
 File Encoding         : 65001

 Date: 29/06/2023 15:27:05
*/

SET NAMES utf8mb4;
SET
FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for ppp_admin_account
-- ----------------------------
DROP TABLE IF EXISTS `ppp_admin_account`;
CREATE TABLE `ppp_admin_account`
(
    `ID`                 varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'ID',
    `USERNAME`           varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户名',
    `PASSWORD`           varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '密码',
    `AME_ID`             varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'AME登录ID',
    `STATUS`             int(11) NULL DEFAULT 1 COMMENT '状态, 0:禁用; 1:启用',
    `IS_SUPER`           int(11) NULL DEFAULT 0 COMMENT '是否是超级管理员, 0:否; 1:是',
    `SUPER_FORUM`        int(11) NULL DEFAULT 0 COMMENT '是否是论坛超级管理员, 0:否; 1:是',
    `CREATED_BY`         varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人ID',
    `CREATED_TIME`       datetime(3) NULL DEFAULT NULL COMMENT '创建时间',
    `LAST_MODIFIED_BY`   varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '最后修改人ID',
    `LAST_MODIFIED_TIME` datetime(3) NULL DEFAULT NULL COMMENT '最后修改时间',
    PRIMARY KEY (`ID`) USING BTREE,
    INDEX                `PPP_ADMIN_ACCOUNT_USERNAME`(`USERNAME`) USING BTREE,
    INDEX                `PPP_ADMIN_ACCOUNT_AME_ID`(`AME_ID`) USING BTREE,
    INDEX                `PPP_ADMIN_ACCOUNT_CREATED_TIME`(`CREATED_TIME`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '管理员账号' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for ppp_annex
-- ----------------------------
DROP TABLE IF EXISTS `ppp_annex`;
CREATE TABLE `ppp_annex`
(
    `ID`            varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'id',
    `ANNEX_TYPE`    varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '附件类型',
    `ANNEX_SIZE`    int(32) NULL DEFAULT NULL COMMENT '附件大小',
    `ORIGINAL_NAME` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '原始名称',
    `STORAGE_NAME`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '存储名称',
    `BUSINESS_TYPE` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '所属业务类型',
    `BUSINESS_ID`   varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '所属业务id',
    `ANNEX_URL`     varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '附件地址',
    `CREATED_BY`    varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建人',
    `CREATED_TIME`  datetime(3) NULL DEFAULT NULL COMMENT '创建时间',
    `UPDATED_BY`    varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新人',
    `UPDATED_TIME`  datetime(3) NULL DEFAULT NULL COMMENT '更新时间',
    `DELETE_FLAG`   int(11) NULL DEFAULT 0 COMMENT '逻辑删除字段（0/1）',
    PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '附件表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for ppp_audit_request
-- ----------------------------
DROP TABLE IF EXISTS `ppp_audit_request`;
CREATE TABLE `ppp_audit_request`
(
    `ID`              varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci  NOT NULL COMMENT '请求ID',
    `REQUEST_IP`      varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci  NOT NULL COMMENT '请求IP',
    `REQUEST_METHOD`  varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci  NOT NULL COMMENT '请求方法',
    `REQUEST_PATH`    varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '请求路径',
    `REQUEST_QUERY`   varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '请求的Query参数',
    `REQUEST_TIME`    datetime(3) NOT NULL COMMENT '请求时间',
    `REQUEST_BY`      varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci  NOT NULL COMMENT '请求人ID',
    `ELAPSED_TIME`    int(11) NOT NULL COMMENT '处理耗时',
    `RESPONSE_STATUS` int(11) NOT NULL COMMENT '响应状态',
    `MODULE_NAME`     varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '模块名称',
    `OPERATION_NAME`  varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '操作名称',
    `HANDLER`         varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Spring的请求Handler',
    `MESSAGE`         varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '日志信息',
    `RESULT_CODE`     int(11) NOT NULL COMMENT '结果码',
    `RESULT_MESSAGE`  varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '结果信息',
    `INSTANCE_TYPE`   varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '实例类型: admin, portal',
    `INSTANCE_IP`     varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '实例IP',
    `TARGET_TYPE`     varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '资源类型',
    `DATA_JSON`       text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '资源详情',
    PRIMARY KEY (`ID`) USING BTREE,
    INDEX             `PPP_AUDIT_REQUEST_REQUEST_BY`(`REQUEST_BY`) USING BTREE,
    INDEX             `PPP_AUDIT_REQUEST_REQUEST_IP`(`REQUEST_IP`) USING BTREE,
    INDEX             `PPP_AUDIT_REQUEST_RESPONSE_STATUS`(`RESPONSE_STATUS`) USING BTREE,
    INDEX             `PPP_AUDIT_REQUEST_RESULT_CODE`(`RESULT_CODE`) USING BTREE,
    INDEX             `PPP_AUDIT_REQUEST_REQUEST_TIME`(`REQUEST_TIME`) USING BTREE,
    INDEX             `PPP_AUDIT_REQUEST_MODULE_NAME`(`MODULE_NAME`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = 'PPP请求审计' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for ppp_auth
-- ----------------------------
DROP TABLE IF EXISTS `ppp_auth`;
CREATE TABLE `ppp_auth`
(
    `ID`                 varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键id',
    `AUTH_NAME`          varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '认证项目名称',
    `AUTH_DESC`          varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '认证项目简介',
    `TYPE`               varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '产品方案分类',
    `COVER_IMG`          varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '认证封面图片',
    `TEMPLATE_ID`        varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '证书模板id',
    `JOIN_COUNT`         int(11) NULL DEFAULT 0 COMMENT '参与人数',
    `EXAM_COUNT`         int(11) NULL DEFAULT 0 COMMENT '考试人数',
    `PASS_COUNT`         int(11) NULL DEFAULT 0 COMMENT '通过人数',
    `EXAM_PASS_SCORE`    int(11) NULL DEFAULT NULL COMMENT '线下考试合格分数',
    `EXAM_MODE`          int(11) NULL DEFAULT 1 COMMENT '考试方式：1-线上考试',
    `IS_INTERNAL`        int(11) NULL DEFAULT 0 COMMENT '是否内部认证：0-否；1-是',
    `HOME_SHOW`          int(11) NULL DEFAULT NULL COMMENT '首页展示：1-产品；2-方案',
    `SORT`               int(11) NULL DEFAULT 100 COMMENT '排序',
    `STATUS`             int(11) NULL DEFAULT 0 COMMENT '状态：0-下架；1-上架',
    `CREATED_BY`         varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建人',
    `CREATED_TIME`       datetime(3) NULL DEFAULT NULL COMMENT '创建时间',
    `LAST_MODIFIED_BY`   varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新人',
    `LAST_MODIFIED_TIME` datetime(3) NULL DEFAULT NULL COMMENT '更新时间',
    PRIMARY KEY (`ID`) USING BTREE,
    INDEX                `idx_page_id`(`TYPE`) USING BTREE,
    INDEX                `idx_template_id`(`TEMPLATE_ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '认证项目' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for ppp_auth_course
-- ----------------------------
DROP TABLE IF EXISTS `ppp_auth_course`;
CREATE TABLE `ppp_auth_course`
(
    `ID`                 varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键id',
    `AUTH_ID`            varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '认证项目id',
    `COURSE_ID`          varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '课程id',
    `CREATED_BY`         varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建人',
    `CREATED_TIME`       datetime(3) NULL DEFAULT NULL COMMENT '创建时间',
    `LAST_MODIFIED_BY`   varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新人',
    `LAST_MODIFIED_TIME` datetime(3) NULL DEFAULT NULL COMMENT '更新时间',
    PRIMARY KEY (`ID`) USING BTREE,
    INDEX                `idx_auth_id`(`AUTH_ID`) USING BTREE,
    INDEX                `idx_course_id`(`COURSE_ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '认证项目-课程关联' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for ppp_auth_paper
-- ----------------------------
DROP TABLE IF EXISTS `ppp_auth_paper`;
CREATE TABLE `ppp_auth_paper`
(
    `ID`                 varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键id',
    `AUTH_ID`            varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '认证项目id',
    `PAPER_ID`           varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '试卷id',
    `CREATED_BY`         varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建人',
    `CREATED_TIME`       datetime(3) NULL DEFAULT NULL COMMENT '创建时间',
    `LAST_MODIFIED_BY`   varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新人',
    `LAST_MODIFIED_TIME` datetime(3) NULL DEFAULT NULL COMMENT '更新时间',
    PRIMARY KEY (`ID`) USING BTREE,
    INDEX                `idx_auth_id`(`AUTH_ID`) USING BTREE,
    INDEX                `idx_paper_id`(`PAPER_ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '认证项目-试卷关联' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for ppp_auth_template
-- ----------------------------
DROP TABLE IF EXISTS `ppp_auth_template`;
CREATE TABLE `ppp_auth_template`
(
    `ID`                   varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键id',
    `TEMPLATE_ID`          varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '模板id',
    `USER_ID`              varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户id',
    `RECORD_ID`            varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '认证记录id',
    `USER_NAME`            varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户姓名',
    `ID_CARD`              varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户证件号',
    `AUTH_NUM`             varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '证书编号',
    `AUTH_NAME`            varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '认证项目名称',
    `ISSUE_DATE`           datetime NULL DEFAULT NULL COMMENT '颁发时间',
    `AUTH_DATE`            datetime NULL DEFAULT NULL COMMENT '认证时间',
    `AUTH_USER_IMG`        varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '认证人照片',
    `ELECTRONIC_SIGNATURE` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '电子签章',
    `ISSUE_UNIT`           varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '签发单位',
    `TEMPLATE_AUTH_NAME`   varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '模板上的认证项目名称',
    `EFFECTIVE_TIME`       datetime NULL DEFAULT NULL COMMENT '有效时间',
    `EXAM_SCORE`           int(11) NULL DEFAULT NULL COMMENT '考试成绩',
    `CREATED_BY`           varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建人',
    `CREATED_TIME`         datetime(3) NULL DEFAULT NULL COMMENT '创建时间',
    `LAST_MODIFIED_BY`     varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新人',
    `LAST_MODIFIED_TIME`   datetime(3) NULL DEFAULT NULL COMMENT '更新时间',
    PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '认证模板记录表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for ppp_business
-- ----------------------------
DROP TABLE IF EXISTS `ppp_business`;
CREATE TABLE `ppp_business`
(
    `ID`                      varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '商机id',
    `BUSINESS_NAME`           varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '商机名称',
    `CHANNEL_ID`              varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '渠道ID',
    `SELL_AME_CODE`           varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'ame账号',
    `CUSTOM_ID`               varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备案客户id',
    `BUSINESS_STAGE`          int(11) NULL DEFAULT NULL COMMENT '商机阶段   1  商机创建   2 商机确认   3商机成熟  4招投标   5商务谈判  6 签署合同',
    `BUSINESS_STAGE_EDIT`     int(11) NULL DEFAULT NULL COMMENT '商机阶段修改值',
    `BUSINESS_SIGN_DATE`      datetime NULL DEFAULT NULL COMMENT '预计签约时间',
    `BUSINESS_SIGN_DATE_EDIT` datetime NULL DEFAULT NULL COMMENT '商机预计签约时间修改值',
    `BUSINESS_AMOUNT`         decimal(8, 2) NULL DEFAULT NULL COMMENT '订单预估金额',
    `BUSINESS_AMOUNT_EDIT`    decimal(8, 2) NULL DEFAULT NULL COMMENT '订单金额修改值',
    `BUSINESS_DRC`            varchar(900) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '商机描述',
    `BUSINESS_DRC_EDIT`       varchar(900) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '商机描述修改值',
    `BUSINESS_STATUS`         int(11) NULL DEFAULT NULL COMMENT '商机状态 1-草稿,2-报备中,3-新增审核中,4-新增销售同意,5-新增销售退回,6-修改审核中,7-新增ame审核同意,8-新增ame退回,9-跟进中,10-已签单,11-商机失效,12-修改渠道销售同意,13-修改销售退回',
    `BUSINESS_DATE`           datetime NULL DEFAULT NULL COMMENT '商机报备日期',
    `SELL_CONFIRM_STATUS`     int(255) NULL DEFAULT NULL COMMENT '销售确认状态  1新增未确认 2新增同意 3新增拒绝 4修改未确认 5修改同意 6修改拒绝',
    `AME_EXAMINE_REMARK`      text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT 'AME审核备注',
    `SELL_CONFIRM_REMARK`     text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '销售确认备注',
    `SELL_CONFIRM_DATE`       datetime NULL DEFAULT NULL COMMENT '销售确认时间',
    `CONFIRM_BY`              varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '确认人',
    `created_by`              varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建人',
    `created_time`            datetime(3) NULL DEFAULT NULL COMMENT '创建时间',
    `last_modified_by`        varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新人',
    `last_modified_time`      datetime(3) NULL DEFAULT NULL COMMENT '更新时间',
    `business_enable`         int(1) UNSIGNED ZEROFILL NULL DEFAULT 0 COMMENT '商机有效无效  0 无效  1  有效 2订单绑定 3 签单成功失效',
    PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '商机信息;' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for ppp_business_product
-- ----------------------------
DROP TABLE IF EXISTS `ppp_business_product`;
CREATE TABLE `ppp_business_product`
(
    `ID`                 varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'id',
    `BUSINESS_ID`        varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '商机id',
    `PRODUCT_CODE`       varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '产品编码',
    `PRODUCT_NAME`       varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '产品名称',
    `STATUS`             int(11) NULL DEFAULT NULL COMMENT '1是主数据  2是修改数据',
    `created_by`         varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建人',
    `created_time`       datetime(3) NULL DEFAULT NULL COMMENT '创建时间',
    `last_modified_by`   varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新人',
    `last_modified_time` datetime(3) NULL DEFAULT NULL COMMENT '更新时间',
    PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '商机关联产品;' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for ppp_business_record
-- ----------------------------
DROP TABLE IF EXISTS `ppp_business_record`;
CREATE TABLE `ppp_business_record`
(
    `ID`                 varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'id',
    `BUSINESS_ID`        varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '商机ID',
    `FOLLOW_DATE`        datetime NULL DEFAULT NULL COMMENT '跟进日期',
    `STATUS`             varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '跟进状态  1审核通过  2审核不通过 3待审核',
    `FOLLOW_REMARK`      varchar(900) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '跟进备注',
    `created_by`         varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建人',
    `created_time`       datetime(3) NULL DEFAULT NULL COMMENT '创建时间',
    `last_modified_by`   varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新人',
    `last_modified_time` datetime(3) NULL DEFAULT NULL COMMENT '更新时间'
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '商机记录表;' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for ppp_catalog_info
-- ----------------------------
DROP TABLE IF EXISTS `ppp_catalog_info`;
CREATE TABLE `ppp_catalog_info`
(
    `ID`                 varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'ID',
    `NAME`               varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '名称',
    `DESCRIPTION`        varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '描述',
    `PARENT_ID`          varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '父级ID',
    `LEVEL`              int(11) NOT NULL COMMENT '层级',
    `SORT`               int(11) NULL DEFAULT 100 COMMENT '顺序',
    `STATUS`             int(11) NOT NULL COMMENT '状态, 0:禁用; 1:启用',
    `IS_LOCK`            int(11) NOT NULL COMMENT '是否锁定, 0:普通 1:锁定',
    `ICON`               varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图标',
    `CREATED_BY`         varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人ID',
    `CREATED_TIME`       datetime NULL DEFAULT NULL COMMENT '创建时间',
    `LAST_MODIFIED_BY`   varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '最后修改人ID',
    `LAST_MODIFIED_TIME` datetime NULL DEFAULT NULL COMMENT '最后修改时间',
    PRIMARY KEY (`ID`) USING BTREE,
    INDEX                `PPP_CATALOG_INFO_ID`(`ID`) USING BTREE,
    INDEX                `PPP_CATALOG_INFO_NAME`(`NAME`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '分类信息' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for ppp_catalog_resource
-- ----------------------------
DROP TABLE IF EXISTS `ppp_catalog_resource`;
CREATE TABLE `ppp_catalog_resource`
(
    `ID`                 varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'ID',
    `TYPE`               int(11) NOT NULL COMMENT '分类(1:产品介绍, 2:解决方案, 3:案例, 4.知识库, 5.文档 99:其他)',
    `CATALOG_ID`         varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '分类ID',
    `RESOURCE_ID`        varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '资源ID',
    `CREATED_BY`         varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人ID',
    `CREATED_TIME`       datetime NULL DEFAULT NULL COMMENT '创建时间',
    `LAST_MODIFIED_BY`   varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '最后修改人ID',
    `LAST_MODIFIED_TIME` datetime NULL DEFAULT NULL COMMENT '最后修改时间',
    PRIMARY KEY (`ID`) USING BTREE,
    INDEX                `PPP_CATALOG_RESOURCE_ID`(`ID`) USING BTREE,
    INDEX                `PPP_CATALOG_RESOURCE_TYPE`(`TYPE`) USING BTREE,
    INDEX                `PPP_CATALOG_RESOURCE_CATALOG_ID`(`CATALOG_ID`) USING BTREE,
    INDEX                `PPP_CATALOG_RESOURCE_RESOURCE_ID`(`RESOURCE_ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '分类资源' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for ppp_ccie_template
-- ----------------------------
DROP TABLE IF EXISTS `ppp_ccie_template`;
CREATE TABLE `ppp_ccie_template`
(
    `ID`                   varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键id',
    `TYPE`                 varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '产品方案类别',
    `TEMPLATE_NAME`        varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '模板名称',
    `STATUS`               int(11) NULL DEFAULT 1 COMMENT '模板启用状态 0-不启用 1-启用',
    `TEMPLATE_IMG`         varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '模板照片',
    `TEMPLATE_URL`         varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '楚果模板url',
    `TEMPLATE_AUTH_NAME`   varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '模板上的认证项目名称',
    `IS_ELC_SIGN`          int(11) NULL DEFAULT NULL COMMENT '是否添加电子签章',
    `SIGN_UNIT`            varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '签发单位',
    `AUTH_NUM_RULE`        varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '证书生成规则',
    `TEMPLATE_NUM_NOW`     int(11) NULL DEFAULT 10000 COMMENT '当前模板证书号码',
    `ELECTRONIC_SIGNATURE` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '电子签章',
    `TERM_VALIDITY`        varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '有效期(单位年)',
    `CREATED_BY`           varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建人',
    `CREATED_TIME`         datetime(3) NULL DEFAULT NULL COMMENT '创建时间',
    `LAST_MODIFIED_BY`     varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新人',
    `LAST_MODIFIED_TIME`   datetime(3) NULL DEFAULT NULL COMMENT '更新时间',
    PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '证书模板' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for ppp_channel
-- ----------------------------
DROP TABLE IF EXISTS `ppp_channel`;
CREATE TABLE `ppp_channel`
(
    `ID`               varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'id',
    `COMPANY_ID`       varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '所属公司id',
    `CHANNEL_NAME`     varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '渠道公司名称',
    `CHANNEL_CODE`     varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '渠道统一信用代码',
    `CHANNEL_CONTACTS` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '渠道公司联系人名称',
    `CHANNEL_PHONE`    varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '渠道公司联系人电话',
    `CHANNEL_ADDRESS`  varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '渠道公司联系人地址',
    `CHANNEL_EMAIL`    varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '渠道联系人邮箱',
    `SELL_NAME`        varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '销售名称',
    `SELL_AME_CODE`    varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '销售ame账号',
    `SELL_PHONE`       varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '销售电话',
    `USER_ID`          varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '渠道用户id',
    PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '渠道信息表;' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for ppp_channel_apply
-- ----------------------------
DROP TABLE IF EXISTS `ppp_channel_apply`;
CREATE TABLE `ppp_channel_apply`
(
    `ID`            varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'id',
    `CHANNEL_ID`    varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '渠道公司id',
    `STATUS`        int(11) NULL DEFAULT NULL COMMENT '审核状态（0审核未通过 1审核通过 2销售人员审核 3管理员审核）',
    `ALTER_VALUE`   text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '变更内容',
    `CHANNEL_NAME`  varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '渠道公司名称',
    `SELL_AME_CODE` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '销售ame账号',
    `CREATED_TIME`  datetime(3) NULL DEFAULT NULL COMMENT '创建时间',
    `UPDATED_TIME`  datetime(3) NULL DEFAULT NULL COMMENT '更新时间',
    `UPDATED_BY`    varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新人',
    PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '渠道信息修改申请表;' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for ppp_comment
-- ----------------------------
DROP TABLE IF EXISTS `ppp_comment`;
CREATE TABLE `ppp_comment`
(
    `ID`           varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'id',
    `FORUM_ID`     varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '板块id',
    `USER_ID`      varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户ID',
    `POST_ID`      varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '帖子ID',
    `PID`          varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0' COMMENT '父评论ID',
    `REPLY_ID`     varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '回复用户id',
    `LIKES`        int(11) NULL DEFAULT 0 COMMENT '点赞数',
    `CONTENT`      longtext CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '内容',
    `CREATED_BY`   varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建人',
    `CREATED_TIME` datetime(3) NULL DEFAULT NULL COMMENT '创建时间',
    `DELETE_FLAG`  int(11) NULL DEFAULT 0 COMMENT '逻辑删除字段（0/1）',
    PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '评论表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for ppp_company
-- ----------------------------
DROP TABLE IF EXISTS `ppp_company`;
CREATE TABLE `ppp_company`
(
    `ID`                 varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键：企业id',
    `COMPANY_NAME`       varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '企业名称',
    `CHANNEL_ID`         varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '渠道id',
    `COMPANY_KEY`        varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '企业密钥',
    `IS_INTERNAL`        int(11) NULL DEFAULT 0 COMMENT '是否内部企业(普元)：0-否；1-是',
    `STATUS`             int(11) NULL DEFAULT 1 COMMENT '状态：0-停用；1-启用',
    `CREATED_BY`         varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建人',
    `CREATED_TIME`       datetime(3) NULL DEFAULT NULL COMMENT '创建时间',
    `LAST_MODIFIED_BY`   varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新人',
    `LAST_MODIFIED_TIME` datetime(3) NULL DEFAULT NULL COMMENT '更新时间',
    PRIMARY KEY (`ID`) USING BTREE,
    INDEX                `idx_company_name`(`COMPANY_NAME`) USING BTREE,
    INDEX                `idx_company_key`(`COMPANY_KEY`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '企业' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for ppp_contract
-- ----------------------------
DROP TABLE IF EXISTS `ppp_contract`;
CREATE TABLE `ppp_contract`
(
    `ID`                  varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'id',
    `CONTRACT_NO`         varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '框架合同存储编号',
    `CONTRACT_DATE`       datetime NULL DEFAULT NULL COMMENT '合同签署日期',
    `EXPIRE_DATE`         datetime NULL DEFAULT NULL COMMENT '合同到期日期',
    `CONTRACT_STATUS`     int(11) NULL DEFAULT NULL COMMENT '合同状态（0失效，1生效中，2更新中）',
    `CONTRACT_ATTACHMENT` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '合同附件地址',
    `CHANNEL_ID`          varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '渠道id',
    `CHANNEL_CODE`        varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '渠道信用代码',
    PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '合同信息;' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for ppp_contract_product
-- ----------------------------
DROP TABLE IF EXISTS `ppp_contract_product`;
CREATE TABLE `ppp_contract_product`
(
    `ID`           varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'id',
    `PRODUCT_CODE` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '产品编码',
    `CONTRACT_ID`  varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '合同id',
    `PRODUCT_NAME` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '产品名称',
    PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '产品合同中间表;' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for ppp_course
-- ----------------------------
DROP TABLE IF EXISTS `ppp_course`;
CREATE TABLE `ppp_course`
(
    `ID`                 varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键id',
    `NAME`               varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '名称',
    `COURSE_DESC`        varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '描述',
    `TYPE`               varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '产品方案分类',
    `SORT`               int(11) NULL DEFAULT 100 COMMENT '顺序',
    `COVER_IMG`          varchar(1024) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '封面图片',
    `PARENT_ID`          varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '父id',
    `LEVEL_TYPE`         int(11) NULL DEFAULT NULL COMMENT '层级类型：1-课程；2-章节；3-小节',
    `RESOURCE`           varchar(1024) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '资源',
    `DURATION`           decimal(8, 2) NULL DEFAULT NULL COMMENT '资源时长（分钟）',
    `NODE_COUNT`         int(11) NULL DEFAULT 0 COMMENT '课程小节数',
    `STUDY_COUNT`        int(11) NULL DEFAULT 0 COMMENT '学习人数',
    `COMPLETE_COUNT`     int(11) NULL DEFAULT 0 COMMENT '完成人数',
    `STATUS`             int(11) NULL DEFAULT 1 COMMENT '状态：0-停用；1-启用',
    `CREATED_BY`         varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建人',
    `CREATED_TIME`       datetime(3) NULL DEFAULT NULL COMMENT '创建时间',
    `LAST_MODIFIED_BY`   varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新人',
    `LAST_MODIFIED_TIME` datetime(3) NULL DEFAULT NULL COMMENT '更新时间',
    PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '培训课程' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for ppp_custom_apply
-- ----------------------------
DROP TABLE IF EXISTS `ppp_custom_apply`;
CREATE TABLE `ppp_custom_apply`
(
    `ID`                  varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'id',
    `FILL_CUSTOM_ID`      varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '报备记录id',
    `DELAY_MONTH`         int(11) NULL DEFAULT NULL COMMENT '延期月数;单位为月',
    `DELAY_STATUS`        int(11) NULL DEFAULT NULL COMMENT '审核状态:1-销售延期审批、2-阿米加延期审批、3-延期成功、4-延期失败',
    `DELAY_FILE`          varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '延期材料',
    `REMARK`              varchar(900) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
    `SELL_CONFIRM_STATUS` int(255) NULL DEFAULT NULL COMMENT '销售确认状态:0-未确认、1-已确认',
    `SELL_CONFIRM_REMARK` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '销售确认备注',
    `SELL_CONFIRM_DATE`   datetime NULL DEFAULT NULL COMMENT '销售确认时间',
    `AUDITE_BY`           varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '审核人',
    `AME_CONFIRM_REMARK`  text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT 'ame确认备注',
    `CREATED_BY`          varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人ID',
    `CREATED_TIME`        datetime(3) NULL DEFAULT NULL COMMENT '创建时间',
    `LAST_MODIFIED_BY`    varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '最后修改人ID',
    `LAST_MODIFIED_TIME`  datetime(3) NULL DEFAULT NULL COMMENT '最后修改时间',
    PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '客户延期申请表;' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for ppp_custom_business
-- ----------------------------
DROP TABLE IF EXISTS `ppp_custom_business`;
CREATE TABLE `ppp_custom_business`
(
    `ID`          varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'id',
    `BUSINESS_ID` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '商机id',
    `CUSTOM_CODE` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '客户信用代码',
    PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '客户关联商机表;' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for ppp_customer
-- ----------------------------
DROP TABLE IF EXISTS `ppp_customer`;
CREATE TABLE `ppp_customer`
(
    `ID`                 varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'ID',
    `NAME`               varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
    `SUMMARY`            varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '概要介绍',
    `STATUS`             int(11) NULL DEFAULT NULL COMMENT '状态, 0:禁用; 1:启用',
    `GROUP_ID`           varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '组ID',
    `MAIN_CASE_ID`       varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '精选首推案例ID, 对应PAGE ID',
    `TAG_IDS`            varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'TAG IDS',
    `LEVEL`              int(11) NULL DEFAULT 0 COMMENT '客户级别, 大于0都是精选',
    `ORDERED_VALUE`      int(11) NULL DEFAULT 0 COMMENT '排序值, 从小到大排列',
    `LOGO`               varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'LOGO图片',
    `CREATED_BY`         varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人ID',
    `CREATED_TIME`       datetime(3) NULL DEFAULT NULL COMMENT '创建时间',
    `LAST_MODIFIED_BY`   varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '最后修改人ID',
    `LAST_MODIFIED_TIME` datetime(3) NULL DEFAULT NULL COMMENT '最后修改时间',
    `MASKING_NAME`       varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
    PRIMARY KEY (`ID`) USING BTREE,
    INDEX                `PPP_CUSTOMER_NAME`(`NAME`) USING BTREE,
    INDEX                `PPP_CUSTOMER_GROUP_ID`(`GROUP_ID`) USING BTREE,
    INDEX                `PPP_CUSTOMER_CASE_ID`(`MAIN_CASE_ID`) USING BTREE,
    INDEX                `PPP_CUSTOMER_STATUS`(`STATUS`) USING BTREE,
    INDEX                `PPP_CUSTOMER_ORDERED_VALUE`(`ORDERED_VALUE`) USING BTREE,
    INDEX                `PPP_CUSTOMER_CREATED_TIME`(`CREATED_TIME`) USING BTREE,
    INDEX                `PPP_CUSTOMER_MAIN_CASE_ID`(`MAIN_CASE_ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '客户' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for ppp_customer_ame_postpone
-- ----------------------------
DROP TABLE IF EXISTS `ppp_customer_ame_postpone`;
CREATE TABLE `ppp_customer_ame_postpone`
(
    `ID`                 varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'id',
    `CUSTOM_ID`          varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '客户ID',
    `SELL_AME_CODE`      varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '销售ame账号',
    `DEVELOP_DATE_BEGIN` datetime NULL DEFAULT NULL COMMENT '开拓有效开始日期',
    `DEVELOP_DATE_END`   datetime NULL DEFAULT NULL COMMENT '开拓有效结束日期',
    `CREATED_BY`         varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人ID',
    `CREATED_TIME`       datetime(3) NULL DEFAULT NULL COMMENT '创建时间',
    `LAST_MODIFIED_BY`   varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '最后修改人ID',
    `LAST_MODIFIED_TIME` datetime(3) NULL DEFAULT NULL COMMENT '最后修改时间',
    PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '客户ame申请延期记录表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for ppp_customer_group
-- ----------------------------
DROP TABLE IF EXISTS `ppp_customer_group`;
CREATE TABLE `ppp_customer_group`
(
    `ID`                 varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'ID',
    `NAME`               varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
    `ORDERED_VALUE`      int(11) NULL DEFAULT 0 COMMENT '排序值, 从小到大排列',
    `CREATED_BY`         varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人ID',
    `CREATED_TIME`       datetime(3) NULL DEFAULT NULL COMMENT '创建时间',
    `LAST_MODIFIED_BY`   varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '最后修改人ID',
    `LAST_MODIFIED_TIME` datetime(3) NULL DEFAULT NULL COMMENT '最后修改时间',
    PRIMARY KEY (`ID`) USING BTREE,
    INDEX                `PPP_CUSTOMER_GROUP_NAME`(`NAME`) USING BTREE,
    INDEX                `PPP_CUSTOMER_GROUP_ORDERED_VALUE`(`ORDERED_VALUE`) USING BTREE,
    INDEX                `PPP_CUSTOMER_GROUP_CREATED_TIME`(`CREATED_TIME`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '客户分组' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for ppp_data_control
-- ----------------------------
DROP TABLE IF EXISTS `ppp_data_control`;
CREATE TABLE `ppp_data_control`
(
    `ID`           varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'id',
    `ADMIN_ID`     varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '管理员id',
    `DATA_ID`      varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '数据Id',
    `TYPE`         varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '类型',
    `CREATED_BY`   varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建人',
    `CREATED_TIME` datetime(3) NULL DEFAULT NULL COMMENT '创建时间',
    `UPDATED_BY`   varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新人',
    `UPDATED_TIME` datetime(3) NULL DEFAULT NULL COMMENT '更新时间',
    PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '数据控制表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for ppp_dict_type
-- ----------------------------
DROP TABLE IF EXISTS `ppp_dict_type`;
CREATE TABLE `ppp_dict_type`
(
    `ID`                 varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键ID',
    `CODE`               varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '字典类型编码',
    `NAME`               varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '字典类型名称',
    `SORT_BY`            decimal(2, 0) NULL DEFAULT NULL COMMENT '排序字段',
    `STATUS`             int(11) NULL DEFAULT 1 COMMENT '状态(0禁用,1启用)',
    `DESCRIPTION`        text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '描述',
    `CREATED_BY`         varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人ID',
    `CREATED_TIME`       datetime(3) NULL DEFAULT NULL COMMENT '创建时间',
    `LAST_MODIFIED_BY`   varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '最后修改人ID',
    `LAST_MODIFIED_TIME` datetime(3) NULL DEFAULT NULL COMMENT '最后修改时间',
    PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '字典类型表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for ppp_dict_type_template
-- ----------------------------
DROP TABLE IF EXISTS `ppp_dict_type_template`;
CREATE TABLE `ppp_dict_type_template`
(
    `ID`          varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
    `CODE`        varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '字典编码',
    `NAME`        varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '字典名称',
    `PARENT_ID`   varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '父字典类型ID',
    `SORT_BY`     decimal(8, 0) NULL DEFAULT NULL COMMENT '排序字段',
    `STATUS`      int(11) NULL DEFAULT 1 COMMENT '状态(0禁用,1启用)',
    `DESCRIPTION` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '描述',
    PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '字典类型模板表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for ppp_fill_customers
-- ----------------------------
DROP TABLE IF EXISTS `ppp_fill_customers`;
CREATE TABLE `ppp_fill_customers`
(
    `ID`                  varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'id',
    `CUSTOM_ID`           varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '客户ID',
    `CHANNEL_ID`          varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '渠道ID',
    `REPORT_DATE`         datetime NULL DEFAULT NULL COMMENT '客户报备日期',
    `REPORT_STATUS`       int(11) NULL DEFAULT NULL COMMENT '客户报备状态:1-待报备、2-销售报备审批、3-阿米加报备审批、4-报备成功、5-报备失败、6-销售延期审批、7-阿米加延期审批、8-延期成功、9-延期失败',
    `CUSTOM_STATUS`       int(11) NULL DEFAULT NULL COMMENT '客户状态;未签单,已签单',
    `REMARK`              varchar(900) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
    `DEVELOP_DATE_BEGIN`  datetime NULL DEFAULT NULL COMMENT '开拓有效开始日期',
    `DEVELOP_DATE_END`    datetime NULL DEFAULT NULL COMMENT '开拓有效结束日期',
    `SELL_CONFIRM_STATUS` int(255) NULL DEFAULT NULL COMMENT '销售确认状态:0-未确认、1-已确认',
    `SELL_CONFIRM_REMARK` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '销售确认备注',
    `SELL_CONFIRM_DATE`   datetime NULL DEFAULT NULL COMMENT '销售确认时间',
    `AUDITE_BY`           varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '确认人',
    `AME_CONFIRM_REMARK`  text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT 'ame确认备注',
    `CREATED_BY`          varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人ID',
    `CREATED_TIME`        datetime(3) NULL DEFAULT NULL COMMENT '创建时间',
    `LAST_MODIFIED_BY`    varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '最后修改人ID',
    `LAST_MODIFIED_TIME`  datetime(3) NULL DEFAULT NULL COMMENT '最后修改时间',
    PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '备案客户信息;' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for ppp_fill_record
-- ----------------------------
DROP TABLE IF EXISTS `ppp_fill_record`;
CREATE TABLE `ppp_fill_record`
(
    `ID`                      varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'ID',
    `FILL_ID`                 varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '开票信息id',
    `FILL_STATUS`             int(32) NULL DEFAULT NULL COMMENT '开票状态:1-待申请、2-审核中、3-申请通过、4-申请驳回',
    `FILL`                    varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '开票发票附件id',
    `ORDER_ID`                varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '订单id集合',
    `FILL_AMOUNT`             decimal(32, 4) NULL DEFAULT 0.0000 COMMENT '开票金额',
    `FILL_DATE`               datetime NULL DEFAULT NULL COMMENT '开票日期',
    `TYPE`                    int(11) NULL DEFAULT 1 COMMENT '开票类型:1-纸质增值税普通、2-纸质增值税专用、3-电子增值税普通、4-电子增值税专用',
    `INVOICE_NAME`            varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '开票公司全称',
    `DUTY_NUM`                varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '税号',
    `BANK`                    varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '开户银行',
    `INVOICE_COMPANY_ADDRESS` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '开票公司地址',
    `INVOICE_PEOPLE`          varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '发票邮寄联系人',
    `INVOICE_IPHONE`          varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '发票邮寄电话',
    `INVOICE_ADDRESS`         varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '发票邮寄地址',
    `AME_CONFIRM_REMARK`      text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT 'ame确认备注',
    `TRACKING_NUMBER`         varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '纸质发票快递单号',
    `CREATED_BY`              varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人ID',
    `CREATED_TIME`            datetime(3) NULL DEFAULT NULL COMMENT '创建时间',
    `LAST_MODIFIED_BY`        varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '最后修改人ID',
    `LAST_MODIFIED_TIME`      datetime(3) NULL DEFAULT NULL COMMENT '最后修改时间',
    PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '开票记录表;' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for ppp_forum
-- ----------------------------
DROP TABLE IF EXISTS `ppp_forum`;
CREATE TABLE `ppp_forum`
(
    `ID`           varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'id',
    `NAME`         varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
    `LABEL_ID`     varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '分类ID',
    `PID`          varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '父id',
    `STATUS`       int(11) NULL DEFAULT 1 COMMENT '状态',
    `TO_REVIEW`    int(11) NULL DEFAULT 0 COMMENT '是否需要审核',
    `ORDERS`       int(11) NULL DEFAULT 1 COMMENT '排序',
    `CREATED_BY`   varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建人',
    `CREATED_TIME` datetime(3) NULL DEFAULT NULL COMMENT '创建时间',
    `UPDATED_BY`   varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新人',
    `UPDATED_TIME` datetime(3) NULL DEFAULT NULL COMMENT '更新时间',
    `DELETE_FLAG`  int(11) NULL DEFAULT 0 COMMENT '逻辑删除字段（0/1）',
    PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '板块' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for ppp_instance
-- ----------------------------
DROP TABLE IF EXISTS `ppp_instance`;
CREATE TABLE `ppp_instance`
(
    `ID`                  varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'ID',
    `IP`                  varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '实例IP',
    `PORT`                int(11) NOT NULL COMMENT '实例端口',
    `PID`                 int(11) NULL DEFAULT NULL COMMENT '进程ID',
    `STATUS`              int(11) NOT NULL DEFAULT 1 COMMENT '状态, 0:停止; 1:运行',
    `IS_BLOCKED`          int(11) NOT NULL DEFAULT 0 COMMENT '是否被封, 被封的IP不允许注册',
    `TYPE`                varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '类型',
    `DESCRIPTION`         varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '描述',
    `VERSION`             varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '实例的版本',
    `VERSION_DESC`        varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '实例的版本描述',
    `STARTUP_TIME`        datetime(3) NULL DEFAULT NULL COMMENT '启动时间',
    `LAST_HEARTBEAT_TIME` datetime(3) NULL DEFAULT NULL COMMENT '最后心跳时间',
    `CREATED_TIME`        datetime(3) NULL DEFAULT NULL COMMENT '创建时间',
    `LAST_MODIFIED_TIME`  datetime(3) NULL DEFAULT NULL COMMENT '最后修改时间',
    PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = 'PPP实例' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for ppp_invoice
-- ----------------------------
DROP TABLE IF EXISTS `ppp_invoice`;
CREATE TABLE `ppp_invoice`
(
    `ID`                      varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'id',
    `INVOICE_NAME`            varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '开票公司全称',
    `DUTY_NUM`                varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '税号',
    `BANK`                    varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '开户银行',
    `INVOICE_COMPANY_ADDRESS` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '开票公司地址',
    `INVOICE_PEOPLE`          varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '发票邮寄联系人',
    `INVOICE_IPHONE`          varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '发票邮寄电话',
    `INVOICE_ADDRESS`         varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '发票邮寄地址',
    `CHANNEL_ID`              varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '渠道id',
    PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '开票信息;' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for ppp_know_info
-- ----------------------------
DROP TABLE IF EXISTS `ppp_know_info`;
CREATE TABLE `ppp_know_info`
(
    `ID`                 varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci  NOT NULL COMMENT 'ID',
    `COVER`              varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '封面',
    `TITLE`              varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '标题',
    `CONTENT`            mediumtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '内容',
    `RESUME`             varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '简介',
    `DOWNLOADS`          text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '附件',
    `ORDERED_VALUE`      int(11) NULL DEFAULT 100 COMMENT '排序值, 从小到大排列',
    `AUTHOR`             varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '作者',
    `STATUS`             int(11) NULL DEFAULT NULL COMMENT '状态, 0:禁用; 1:启用',
    `VIEW_COUNT`         int(11) NULL DEFAULT 0 COMMENT '浏览次数',
    `FAVORITE_COUNT`     int(11) NULL DEFAULT 0 COMMENT '收藏次数',
    `SHARE_COUNT`        int(11) NULL DEFAULT 0 COMMENT '分享次数',
    `DATA_SOURCES`       int(11) NULL DEFAULT 1 COMMENT '数据来源, 1:系统录入; 2:阿米加',
    `CREATED_BY`         varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人ID',
    `CREATED_TIME`       datetime NULL DEFAULT NULL COMMENT '创建时间',
    `LAST_MODIFIED_BY`   varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '最后修改人ID',
    `LAST_MODIFIED_TIME` datetime NULL DEFAULT NULL COMMENT '最后修改时间',
    PRIMARY KEY (`ID`) USING BTREE,
    INDEX                `PPP_KNOW_INFO_TITLE`(`TITLE`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '知识库信息' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for ppp_license_product
-- ----------------------------
DROP TABLE IF EXISTS `ppp_license_product`;
CREATE TABLE `ppp_license_product`
(
    `ID`           varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'id',
    `LICENSE_ID`   varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'license申请id',
    `PRODUCT_CODE` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '产品编码',
    `PRODUCT_NAME` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '产品名称',
    `PRODUCT_NUM`  int(11) NULL DEFAULT NULL COMMENT '产品数量',
    `ORDER_NO`     varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '订单编码',
    PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = 'license申请产品表;' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for ppp_license_report
-- ----------------------------
DROP TABLE IF EXISTS `ppp_license_report`;
CREATE TABLE `ppp_license_report`
(
    `ID`                     varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'id',
    `ORDER_NO`               varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '订单编号',
    `ORDER_CUSTOM_NAME`      varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '订单客户名称',
    `ORDER_PRODUCT_NAME`     varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'license对应项目名',
    `STATUS`                 int(11) NULL DEFAULT NULL COMMENT '申请状态（0未通过，1通过，2暂存，3待审核）',
    `ORDER_CONTACT_NAME`     varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '客户联系人名称',
    `IS_LICENSE_CONTACT`     int(11) NULL DEFAULT NULL COMMENT '是否为license接受人',
    `CUSTOM_CONTACT_PHONE`   varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '客户联系人电话',
    `CUSTOM_CONTACT_EMAIL`   varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '客户联系人邮箱',
    `CUSTOM_CONTACT_ADDRESS` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '客户联系人地址',
    `channel_id`             varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '渠道id',
    `remark`                 varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'ame审核备注',
    `created_by`             varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建人',
    `created_time`           datetime(3) NULL DEFAULT NULL COMMENT '创建时间',
    `updated_by`             varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新人',
    `updated_time`           datetime(3) NULL DEFAULT NULL COMMENT '更新时间',
    PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = 'license申请记录表;' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for ppp_lock
-- ----------------------------
DROP TABLE IF EXISTS `ppp_lock`;
CREATE TABLE `ppp_lock`
(
    `ID`            varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '锁ID',
    `STATUS`        int(11) NULL DEFAULT NULL COMMENT '锁状态, 0:未锁定; 1:已锁定',
    `LOCKED_BY`     varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '锁持有者, 对应实例ID',
    `LOCKED_TIME`   datetime NULL DEFAULT NULL COMMENT '加锁时间',
    `UNLOCKED_TIME` datetime NULL DEFAULT NULL COMMENT '释放锁时间',
    `VERSION`       bigint(20) NULL DEFAULT NULL COMMENT '乐观锁版本',
    PRIMARY KEY (`ID`) USING BTREE,
    INDEX           `IDX_PPP_LOCK_STATUS`(`STATUS`) USING BTREE,
    INDEX           `IDX_PPP_LOCK_LOCKED_BY`(`LOCKED_BY`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = 'PPP分布式锁' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for ppp_menu
-- ----------------------------
DROP TABLE IF EXISTS `ppp_menu`;
CREATE TABLE `ppp_menu`
(
    `ID`                 varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键id',
    `PARENT_ID`          varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '父菜单ID，一级菜单为0',
    `NAME`               varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '菜单名称',
    `URL`                varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '菜单URL',
    `PERMS`              varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '授权(多个用逗号分隔，如：user:list,user:create)',
    `TYPE`               int(11) NULL DEFAULT NULL COMMENT '类型   0：目录   1：菜单   2：按钮',
    `ICON`               varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '菜单图标',
    `ORDER_NUM`          int(11) NULL DEFAULT NULL COMMENT '排序',
    `STATUS`             int(11) NULL DEFAULT 1 COMMENT '状态：0-禁用；1-启用',
    `CREATED_BY`         varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建人',
    `CREATED_TIME`       datetime(3) NULL DEFAULT NULL COMMENT '创建时间',
    `LAST_MODIFIED_BY`   varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新人',
    `LAST_MODIFIED_TIME` datetime(3) NULL DEFAULT NULL COMMENT '更新时间',
    PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '菜单管理' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for ppp_message
-- ----------------------------
DROP TABLE IF EXISTS `ppp_message`;
CREATE TABLE `ppp_message`
(
    `ID`                 varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键id',
    `MSG_TYPE`           int(11) NOT NULL COMMENT '消息分类：1-系统消息；2-服务提醒；3-客户动态',
    `MSG_CODE`           varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '消息代码',
    `TITLE`              varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '标题',
    `SUMMARY`            varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '摘要',
    `CONTENT`            mediumtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '内容',
    `MSG_STATUS`         int(11) NULL DEFAULT 1 COMMENT '消息状态：1-待推送；2-已推送；3-已撤销',
    `PUSH_BY`            varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '推送人',
    `PUSH_TIME`          datetime(3) NULL DEFAULT NULL COMMENT '推送时间',
    `UNDO_BY`            varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '撤销人',
    `UNDO_TIME`          datetime(3) NULL DEFAULT NULL COMMENT '撤销时间',
    `CREATED_BY`         varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建人',
    `CREATED_TIME`       datetime(3) NULL DEFAULT NULL COMMENT '创建时间',
    `LAST_MODIFIED_BY`   varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新人',
    `LAST_MODIFIED_TIME` datetime(3) NULL DEFAULT NULL COMMENT '更新时间',
    PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '站内消息' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for ppp_message_model
-- ----------------------------
DROP TABLE IF EXISTS `ppp_message_model`;
CREATE TABLE `ppp_message_model`
(
    `ID`                 varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'id',
    `MESSAGE_CODE`       varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '消息模板代码',
    `MESSAGE_TYPE`       varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '消息类型(公告和消息),1:公告; 2:消息(私人);',
    `MESSAGE_CONTENT`    varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '消息模板内容',
    `CREATED_BY`         varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建人',
    `CREATED_TIME`       datetime NULL DEFAULT NULL COMMENT '创建时间',
    `LAST_MODIFIED_BY`   varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新人',
    `LAST_MODIFIED_TIME` datetime NULL DEFAULT NULL COMMENT '更新时间',
    PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '消息模板' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for ppp_message_record
-- ----------------------------
DROP TABLE IF EXISTS `ppp_message_record`;
CREATE TABLE `ppp_message_record`
(
    `ID`                 varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'id',
    `MESSAGE_MODEL_ID`   varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '消息模板id',
    `MESSAGE_TYPE`       varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '消息类型(公告和消息),1:公告; 2:消息(私人);',
    `MESSAGE_CONTENT`    varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '消息内容',
    `MESSAGE_STATUS`     int(8) NULL DEFAULT 1 COMMENT '消息状态,1:有效；0:无效;',
    `MESSAGE_USER_ID`    varchar(46) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '渠道账号/ame账号用户ID',
    `MESSAGE_TIME`       datetime NULL DEFAULT NULL COMMENT '消息日期',
    `READ_STATUS`        int(8) NULL DEFAULT 1 COMMENT '是否已读;1:未读；2：已读；',
    `READ_TIME`          datetime NULL DEFAULT NULL COMMENT '读取时间',
    `CREATED_BY`         varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建人',
    `CREATED_TIME`       datetime NULL DEFAULT NULL COMMENT '创建时间',
    `LAST_MODIFIED_BY`   varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新人',
    `LAST_MODIFIED_TIME` datetime NULL DEFAULT NULL COMMENT '更新时间',
    PRIMARY KEY (`ID`) USING BTREE,
    INDEX                `idx_msg_user_id`(`MESSAGE_USER_ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '信息表记录表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for ppp_message_record_people
-- ----------------------------
DROP TABLE IF EXISTS `ppp_message_record_people`;
CREATE TABLE `ppp_message_record_people`
(
    `ID`                 varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'id',
    `MESSAGE_RECORD_ID`  varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '消息记录id',
    `MESSAGE_STATUS`     int(8) NULL DEFAULT 1 COMMENT '是否有效,1:有效；0:无效;',
    `MESSAGE_USER_ID`    varchar(46) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '渠道账号/ame账号用户ID',
    `READ_STATUS`        int(8) NULL DEFAULT NULL COMMENT '是否已读;1:未读；2：已读；',
    `READ_TIME`          datetime NULL DEFAULT NULL COMMENT '读取时间',
    `CREATED_BY`         varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建人',
    `CREATED_TIME`       datetime NULL DEFAULT NULL COMMENT '创建时间',
    `LAST_MODIFIED_BY`   varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新人',
    `LAST_MODIFIED_TIME` datetime NULL DEFAULT NULL COMMENT '更新时间',
    PRIMARY KEY (`ID`) USING BTREE,
    INDEX                `idx_msg_user_id`(`MESSAGE_USER_ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '信息公告阅读记录表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for ppp_operation_log
-- ----------------------------
DROP TABLE IF EXISTS `ppp_operation_log`;
CREATE TABLE `ppp_operation_log`
(
    `ID`                 varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'ID',
    `APP_TYPE`           int(11) NULL DEFAULT NULL COMMENT '应用类型（1:admin，2:portal）',
    `OP_TYPE`            int(11) NULL DEFAULT NULL COMMENT '操作类型（0:登录，1:增加，2:删除，3:修改，4:查询，5:查看，99:其他）',
    `MODULE`             varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '操作模块',
    `ACTION`             varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '操作内容',
    `OP_CLASS`           varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '操作类',
    `REQUEST_URL`        varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '请求地址',
    `REQUEST_IP`         varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '请求IP',
    `OP_USER`            varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '操作人',
    `PROCESS_TIME`       int(11) NULL DEFAULT NULL COMMENT '处理时间',
    `CREATED_BY`         varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人ID',
    `CREATED_TIME`       datetime NULL DEFAULT NULL COMMENT '创建时间',
    `LAST_MODIFIED_BY`   varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '最后修改人ID',
    `LAST_MODIFIED_TIME` datetime NULL DEFAULT NULL COMMENT '最后修改时间',
    PRIMARY KEY (`ID`) USING BTREE,
    INDEX                `PPP_OPERATION_LOG_ID`(`ID`) USING BTREE,
    INDEX                `PPP_OPERATION_LOG_APP_TYPE`(`APP_TYPE`) USING BTREE,
    INDEX                `PPP_OPERATION_LOG_OP_TYPE`(`OP_TYPE`) USING BTREE,
    INDEX                `PPP_OPERATION_LOG_CREATED_BY`(`CREATED_BY`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '操作日志' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for ppp_order
-- ----------------------------
DROP TABLE IF EXISTS `ppp_order`;
CREATE TABLE `ppp_order`
(
    `ID`                  varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'ID',
    `CHANNEL_ID`          varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '渠道ID',
    `ORDER_NO`            varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '订单编号',
    `ORDER_TYPE`          int(11) NULL DEFAULT NULL COMMENT '订单类型,1:协议价,2:非协议价;',
    `BUSINESS_ID`         varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '商机id',
    `CUSTOM_ID`           varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '最终客户ID',
    `CUSTOM_NAME`         varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '最终客户名称',
    `CUSTOM_CODE`         varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '最终客户信用代码',
    `ORDER_DATE`          datetime NULL DEFAULT NULL COMMENT '订单日期',
    `ORDER_STATUS`        int(11) NULL DEFAULT NULL COMMENT '订单状态；1：草稿 ；\n2：待销售确认 ；\n3：待ame+审核；\n4：下单成功；\n5：待再次付款 ；\n7：ame+审核未通过 ；\n6：ame+审核中；\n8：ame+再次审核中 ；\n9：订单完成；\n10 ：销售退回；\n11 ：ame+退回 ；',
    `ORDER_PAY_STATUS`    int(11) NULL DEFAULT NULL COMMENT '付款状态，1:未付款;\n2:付款中;\n3:部分付款;\n4:付款完成;',
    `ORDER_AMOUNT`        decimal(32, 4) NULL DEFAULT 0.0000 COMMENT '订单总金额',
    `TAX_ORDER_AMOUNT`    decimal(32, 4) NULL DEFAULT NULL COMMENT '订单总价(含税）',
    `ORDER_SETTLE_AMOUNT` decimal(32, 4) NULL DEFAULT 0.0000 COMMENT '订单付款总金额=实际+返利',
    `ORDER_PAY_AMOUNT`    decimal(32, 4) NULL DEFAULT 0.0000 COMMENT '订单已付金额',
    `ORDER_REBATE_AMOUNT` decimal(32, 4) NULL DEFAULT 0.0000 COMMENT '订单使用返利金额',
    `ORDER_UNPAY_AMOUNT`  decimal(32, 4) NULL DEFAULT 0.0000 COMMENT '订单未付金额',
    `ORDER_CONTRACT`      varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '订单合同',
    `MEDIUM_ADDRESS`      varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '介质地址',
    `ORDER_PO`            varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '电子订单',
    `FINISH_DATE`         datetime NULL DEFAULT NULL COMMENT '订单付款完成时间',
    `SELL_CONFIRM_DATE`   datetime NULL DEFAULT NULL COMMENT '销售确认时间',
    `ADUIT_BY`            varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '确认人',
    `SELL_CONFIRM_STATUS` int(255) NULL DEFAULT NULL COMMENT '销售确认状态；1：未确认；2：已确认；',
    `SELL_CONFIRM_REMARK` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '销售确认备注',
    `IS_LOCK`             int(8) NULL DEFAULT 0 COMMENT '是否锁定：0未锁定，1已锁定',
    `CREATED_BY`          varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
    `CREATED_TIME`        datetime NULL DEFAULT NULL COMMENT '创建时间',
    `LAST_MODIFIED_BY`    varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '最后修改人',
    `LAST_MODIFIED_TIME`  datetime NULL DEFAULT NULL COMMENT '最后修改时间',
    PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '订单信息;' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for ppp_order_payment_flow
-- ----------------------------
DROP TABLE IF EXISTS `ppp_order_payment_flow`;
CREATE TABLE `ppp_order_payment_flow`
(
    `ID`                   varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'ID',
    `FLOW_TYPE`            varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '流水类型：1:发起付款时(付款记录表),2:AME+付款通过时,',
    `SETTLE_ID`            varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '付款记录ID',
    `ORDER_ID`             varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '订单ID',
    `ORDER_AMOUNT`         decimal(32, 4) NULL DEFAULT 0.0000 COMMENT '订单总金额',
    `TAX_ORDER_AMOUNT`     decimal(32, 4) NULL DEFAULT NULL COMMENT '订单总价(含税）',
    `BEFORE_PAY_AMOUNT`    decimal(32, 4) NULL DEFAULT 0.0000 COMMENT '订单实际付款金额（之前）',
    `BEFORE_UNPAY_AMOUNT`  decimal(32, 4) NULL DEFAULT 0.0000 COMMENT '订单未付金额（之前）',
    `BEFORE_REBATE_AMOUNT` decimal(32, 4) NULL DEFAULT 0.0000 COMMENT '订单使用返利金额（之前）',
    `BEFORE_SETTLE_AMOUNT` decimal(32, 4) NULL DEFAULT 0.0000 COMMENT '订单付款总金额=实际付款+返利（之前）',
    `PAY_AMOUNT`           decimal(32, 4) NULL DEFAULT 0.0000 COMMENT '订单实际金额（之后）',
    `UNPAY_AMOUNT`         decimal(32, 4) NULL DEFAULT 0.0000 COMMENT '订单未付金额（之后）',
    `REBATE_AMOUNT`        decimal(32, 4) NULL DEFAULT 0.0000 COMMENT '订单使用返利金额（之后）',
    `SETTLE_AMOUNT`        decimal(32, 4) NULL DEFAULT 0.0000 COMMENT '订单付款总金额=实际付款+返利（之后）',
    `THIS_PAY_AMOUNT`      decimal(32, 4) NULL DEFAULT 0.0000 COMMENT '订单实际付款金额（本次）',
    `THIS_REBATE_AMOUNT`   decimal(32, 4) NULL DEFAULT 0.0000 COMMENT '订单使用返利金额（本次）',
    `THIS_SETTLE_AMOUNT`   decimal(32, 4) NULL DEFAULT 0.0000 COMMENT '订单付款金额=实际付款+返利（本次）',
    `CREATED_BY`           varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
    `CREATED_TIME`         datetime NULL DEFAULT NULL COMMENT '创建时间',
    `LAST_MODIFIED_BY`     varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '最后修改人',
    `LAST_MODIFIED_TIME`   datetime NULL DEFAULT NULL COMMENT '最后修改时间',
    PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '订单付款金额变动流水表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for ppp_order_product
-- ----------------------------
DROP TABLE IF EXISTS `ppp_order_product`;
CREATE TABLE `ppp_order_product`
(
    `ID`                  varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'id',
    `ORDER_ID`            varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '订单id',
    `PRODUCT_CODE`        varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '产品代码',
    `PRODUCT_NAME`        varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '产品名称',
    `PRODUCT_TYPE`        int(8) NULL DEFAULT NULL COMMENT '产品类型：1 产品； 2 人力',
    `PRODUCT_UNIT`        varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '单位',
    `PRODUCT_NUM`         int(11) NULL DEFAULT 0 COMMENT '数量',
    `APPLIED_NUM`         int(11) NULL DEFAULT 0 COMMENT '已申请数量',
    `PRODUCT_PRICE`       decimal(10, 2) NULL DEFAULT NULL COMMENT '单价',
    `PRODUCT_TAX_RATE`    decimal(10, 0) NULL DEFAULT NULL COMMENT '税率',
    `TAX_PRODUCT_AMOUNT`  decimal(16, 4) NULL DEFAULT 0.0000 COMMENT '产品总价(含税）',
    `REAL_PRODUCT_AMOUNT` decimal(16, 4) NULL DEFAULT 0.0000 COMMENT '实际产品总价(非协议价时即产品含税总价）',
    `CREATED_BY`          varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
    `CREATED_TIME`        datetime NULL DEFAULT NULL COMMENT '创建时间',
    `LAST_MODIFIED_BY`    varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '最后修改人',
    `LAST_MODIFIED_TIME`  datetime NULL DEFAULT NULL COMMENT '最后修改时间',
    PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '订单关联产品;' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for ppp_page
-- ----------------------------
DROP TABLE IF EXISTS `ppp_page`;
CREATE TABLE `ppp_page`
(
    `ID`                 varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'ID',
    `TITLE`              varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标题',
    `TYPE`               int(11) NULL DEFAULT 1 COMMENT '类型,  1:产品介绍; 2:解决方案; 3:案例',
    `SUMMARY`            varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '概要介绍',
    `CONTENT_TYPE`       int(11) NULL DEFAULT NULL COMMENT '内容类型,  1:富文本',
    `STATUS`             int(11) NULL DEFAULT NULL COMMENT '状态, 0:禁用; 1:启用',
    `GROUP_ID`           varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '组ID',
    `HEAD_PC_IMG`        varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'PC头部图片',
    `HEAD_MOBILE_IMG`    varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Mobile头部图片',
    `COVER_IMG`          varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '封面图片',
    `ATTACHMENTS`        varchar(1200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '下载附件',
    `CUSTOMER_ID`        varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '客户ID',
    `ORDERED_VALUE`      int(11) NULL DEFAULT 0 COMMENT '排序值, 从小到大排列',
    `VIEW_COUNT`         int(11) NULL DEFAULT 0 COMMENT '浏览次数',
    `SHARE_COUNT`        int(11) NULL DEFAULT 0 COMMENT '分享次数',
    `FAVORITE_COUNT`     int(11) NULL DEFAULT 0 COMMENT '收藏次数',
    `VERSION`            bigint(20) NULL DEFAULT NULL COMMENT '版本',
    `PUBLISHED_BY`       varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '发布人ID',
    `PUBLISHED_TIME`     datetime(3) NULL DEFAULT NULL COMMENT '发布时间',
    `CREATED_BY`         varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人ID',
    `CREATED_TIME`       datetime(3) NULL DEFAULT NULL COMMENT '创建时间',
    `LAST_MODIFIED_BY`   varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '最后修改人ID',
    `LAST_MODIFIED_TIME` datetime(3) NULL DEFAULT NULL COMMENT '最后修改时间',
    `IS_MASKING`         int(11) NULL DEFAULT NULL,
    `HOME_SHOW`          int(11) NULL DEFAULT 0 COMMENT '状态, 0:不展示; 1:展示',
    PRIMARY KEY (`ID`) USING BTREE,
    INDEX                `PPP_PAGE_TYPE`(`TYPE`) USING BTREE,
    INDEX                `PPP_PAGE_CUSTOMER_ID`(`CUSTOMER_ID`) USING BTREE,
    INDEX                `PPP_PAGE_TITLE`(`TITLE`) USING BTREE,
    INDEX                `PPP_PAGE_STATUS`(`STATUS`) USING BTREE,
    INDEX                `PPP_PAGE_ORDERED_VALUE`(`ORDERED_VALUE`) USING BTREE,
    INDEX                `PPP_PAGE_CREATED_TIME`(`CREATED_TIME`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '页面' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for ppp_page_catalog
-- ----------------------------
DROP TABLE IF EXISTS `ppp_page_catalog`;
CREATE TABLE `ppp_page_catalog`
(
    `ID`                 varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'ID',
    `NAME`               varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '名称',
    `DESCRIPTION`        varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '描述',
    `PARENT_ID`          varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '父级ID',
    `LEVEL`              int(11) NOT NULL COMMENT '层级',
    `SORT`               int(11) NULL DEFAULT 100 COMMENT '顺序',
    `STATUS`             int(11) NOT NULL COMMENT '状态, 0:禁用; 1:启用',
    `IS_LOCK`            int(11) NOT NULL COMMENT '是否锁定, 0:普通 1:锁定',
    `ICON`               varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图标',
    `CREATED_BY`         varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人ID',
    `CREATED_TIME`       datetime(3) NULL DEFAULT NULL COMMENT '创建时间',
    `LAST_MODIFIED_BY`   varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '最后修改人ID',
    `LAST_MODIFIED_TIME` datetime(3) NULL DEFAULT NULL COMMENT '最后修改时间',
    PRIMARY KEY (`ID`) USING BTREE,
    INDEX                `PPP_PAGE_CATALOG_NAME`(`NAME`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '页面子分类' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for ppp_page_content
-- ----------------------------
DROP TABLE IF EXISTS `ppp_page_content`;
CREATE TABLE `ppp_page_content`
(
    `PAGE_ID` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '页面ID',
    `CONTENT` mediumtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '页面内容',
    PRIMARY KEY (`PAGE_ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '页面内容' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for ppp_page_group
-- ----------------------------
DROP TABLE IF EXISTS `ppp_page_group`;
CREATE TABLE `ppp_page_group`
(
    `ID`                 varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'ID',
    `NAME`               varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
    `ORDERED_VALUE`      int(11) NULL DEFAULT 0 COMMENT '排序值, 从小到大排列',
    `CREATED_BY`         varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人ID',
    `CREATED_TIME`       datetime(3) NULL DEFAULT NULL COMMENT '创建时间',
    `LAST_MODIFIED_BY`   varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '最后修改人ID',
    `LAST_MODIFIED_TIME` datetime(3) NULL DEFAULT NULL COMMENT '最后修改时间',
    PRIMARY KEY (`ID`) USING BTREE,
    INDEX                `PPP_PAGE_GROUP_NAME`(`NAME`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '页面分组' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for ppp_paper
-- ----------------------------
DROP TABLE IF EXISTS `ppp_paper`;
CREATE TABLE `ppp_paper`
(
    `ID`                 varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键id',
    `PAPER_NAME`         varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '试卷名称',
    `TYPE`               int(11) NOT NULL COMMENT '产品方案分类',
    `GENERATE_TYPE`      int(11) NULL DEFAULT 1 COMMENT '出题方式：1-自动出题；2-人工出题',
    `EXAM_TIME`          int(11) NULL DEFAULT 60 COMMENT '考试时间（分钟）',
    `TOTAL_SCORE`        int(11) NULL DEFAULT 100 COMMENT '试卷总分',
    `PASS_SCORE`         int(11) NULL DEFAULT NULL COMMENT '通过分数',
    `THEME_COUNT`        int(11) NULL DEFAULT NULL COMMENT '题目总数',
    `PAPER_CONTENT`      text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '试卷内容（人工出题才有，json格式存储）',
    `IS_REPEAT`          int(11) NULL DEFAULT 1 COMMENT '是否允许重复考试：0-否；1-是',
    `EXAM_NUM`           int(11) NULL DEFAULT NULL COMMENT '允许重复考试次数',
    `STATUS`             int(11) NULL DEFAULT 1 COMMENT '状态：0-停用；1-启用',
    `SORT`               int(11) NULL DEFAULT 100 COMMENT '试卷排序',
    `CREATED_BY`         varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建人',
    `CREATED_TIME`       datetime(3) NULL DEFAULT NULL COMMENT '创建时间',
    `LAST_MODIFIED_BY`   varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新人',
    `LAST_MODIFIED_TIME` datetime(3) NULL DEFAULT NULL COMMENT '更新时间',
    PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '试卷' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for ppp_paper_standard
-- ----------------------------
DROP TABLE IF EXISTS `ppp_paper_standard`;
CREATE TABLE `ppp_paper_standard`
(
    `ID`                 varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键id',
    `PAPER_ID`           varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '试卷id',
    `THEME_TYPE`         int(11) NULL DEFAULT NULL COMMENT '题目类型：1-单选题；2-多选题；3-是非题；4-填空题',
    `COUNT`              int(11) NULL DEFAULT NULL COMMENT '数量（每种题目类型）',
    `SCORE`              int(11) NULL DEFAULT NULL COMMENT '分值（单个题目）',
    `CREATED_BY`         varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建人',
    `CREATED_TIME`       datetime(3) NULL DEFAULT NULL COMMENT '创建时间',
    `LAST_MODIFIED_BY`   varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新人',
    `LAST_MODIFIED_TIME` datetime(3) NULL DEFAULT NULL COMMENT '更新时间',
    PRIMARY KEY (`ID`) USING BTREE,
    INDEX                `idx_paper_id`(`PAPER_ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '试卷-自动出题标准' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for ppp_pay
-- ----------------------------
DROP TABLE IF EXISTS `ppp_pay`;
CREATE TABLE `ppp_pay`
(
    `TENANT_ID`    varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '租户号',
    `REVISION`     int(11) NULL DEFAULT NULL COMMENT '乐观锁',
    `CREATED_BY`   varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
    `CREATED_TIME` datetime NULL DEFAULT NULL COMMENT '创建时间',
    `UPDATED_BY`   varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新人',
    `UPDATED_TIME` datetime NULL DEFAULT NULL COMMENT '更新时间'
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '付款信息;' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for ppp_portal_menu
-- ----------------------------
DROP TABLE IF EXISTS `ppp_portal_menu`;
CREATE TABLE `ppp_portal_menu`
(
    `ID`                 varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键id',
    `PARENT_ID`          varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '父菜单ID，一级菜单为0',
    `NAME`               varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '菜单名称',
    `URL`                varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '菜单URL',
    `PERMS`              varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '授权(多个用逗号分隔，如：user:list,user:create)',
    `TYPE`               int(11) NULL DEFAULT NULL COMMENT '类型   0：目录   1：菜单   2：按钮',
    `ICON`               varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '菜单图标',
    `ORDER_NUM`          int(11) NULL DEFAULT NULL COMMENT '排序',
    `STATUS`             int(11) NULL DEFAULT 1 COMMENT '状态：0-禁用；1-启用',
    `CREATED_BY`         varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建人',
    `CREATED_TIME`       datetime(3) NULL DEFAULT NULL COMMENT '创建时间',
    `LAST_MODIFIED_BY`   varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新人',
    `LAST_MODIFIED_TIME` datetime(3) NULL DEFAULT NULL COMMENT '更新时间',
    PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '门户菜单' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for ppp_portal_role
-- ----------------------------
DROP TABLE IF EXISTS `ppp_portal_role`;
CREATE TABLE `ppp_portal_role`
(
    `ID`                 varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键id',
    `ROLE_NAME`          varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '角色名称',
    `REMARK`             varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
    `STATUS`             int(11) NULL DEFAULT 1 COMMENT '状态：0-禁用；1-启用',
    `COMPANY_ID`         varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '企业id',
    `CREATED_BY`         varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建人',
    `CREATED_TIME`       datetime(3) NULL DEFAULT NULL COMMENT '创建时间',
    `LAST_MODIFIED_BY`   varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新人',
    `LAST_MODIFIED_TIME` datetime(3) NULL DEFAULT NULL COMMENT '更新时间',
    PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '门户角色' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for ppp_portal_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `ppp_portal_role_menu`;
CREATE TABLE `ppp_portal_role_menu`
(
    `ID`                 varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键id',
    `ROLE_ID`            varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '角色ID',
    `MENU_ID`            varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '菜单ID',
    `CREATED_BY`         varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建人',
    `CREATED_TIME`       datetime(3) NULL DEFAULT NULL COMMENT '创建时间',
    `LAST_MODIFIED_BY`   varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新人',
    `LAST_MODIFIED_TIME` datetime(3) NULL DEFAULT NULL COMMENT '更新时间',
    PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '门户角色菜单关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for ppp_portal_user_role
-- ----------------------------
DROP TABLE IF EXISTS `ppp_portal_user_role`;
CREATE TABLE `ppp_portal_user_role`
(
    `ID`                 varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键id',
    `USER_ID`            varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户ID',
    `ROLE_ID`            varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '角色ID',
    `CREATED_BY`         varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建人',
    `CREATED_TIME`       datetime(3) NULL DEFAULT NULL COMMENT '创建时间',
    `LAST_MODIFIED_BY`   varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新人',
    `LAST_MODIFIED_TIME` datetime(3) NULL DEFAULT NULL COMMENT '更新时间',
    PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '门户用户角色关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for ppp_post
-- ----------------------------
DROP TABLE IF EXISTS `ppp_post`;
CREATE TABLE `ppp_post`
(
    `ID`             varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'id',
    `USER_ID`        varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户id',
    `LABEL_ID`       varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '分类id',
    `FORUM_ID`       varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '板块id',
    `VOTE_ID`        varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '投票ID',
    `COVER`          longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '封面链接',
    `ANNEX`          longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '附件链接',
    `TITLE`          varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标题',
    `CONTENT`        longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '内容',
    `STATUS`         int(11) NULL DEFAULT 1 COMMENT '状态',
    `IS_TOP`         int(11) NULL DEFAULT 0 COMMENT '是否置顶（0/1）',
    `IS_QUALITY`     int(11) NULL DEFAULT 0 COMMENT '是否加精',
    `ENABLE_VOTE`    int(11) NULL DEFAULT 1 COMMENT '开启投票',
    `ENABLE_COMMENT` int(11) NULL DEFAULT 0 COMMENT '开启评论',
    `VIEW_NUM`       int(11) NULL DEFAULT 0 COMMENT '浏览数',
    `COLLECT_NUM`    int(11) NULL DEFAULT 0 COMMENT '收藏数',
    `LIKES`          int(11) NULL DEFAULT 0 COMMENT '点赞数',
    `CREATED_BY`     varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建人',
    `CREATED_TIME`   datetime(3) NULL DEFAULT NULL COMMENT '创建时间',
    `UPDATED_BY`     varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新人',
    `UPDATED_TIME`   datetime(3) NULL DEFAULT NULL COMMENT '更新时间',
    `DELETE_FLAG`    int(11) NULL DEFAULT 0 COMMENT '逻辑删除字段（0/1）',
    PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '帖子' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for ppp_product
-- ----------------------------
DROP TABLE IF EXISTS `ppp_product`;
CREATE TABLE `ppp_product`
(
    `ID`            varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'id',
    `PRODUCT_NAME`  varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '产品名称',
    `PRODUCT_CODE`  varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '产品编码',
    `PRODUCT_TYPE`  int(8) NULL DEFAULT NULL COMMENT '产品类型: 1：产品 ，2:服务，',
    `PRODUCT_UNIT`  varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '单位',
    `PRODUCT_PRICE` decimal(10, 2) NULL DEFAULT NULL COMMENT '单价'
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '产品服务表();' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for ppp_product_license_info
-- ----------------------------
DROP TABLE IF EXISTS `ppp_product_license_info`;
CREATE TABLE `ppp_product_license_info`
(
    `ID`           varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'id',
    `LICENSE_ID`   varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'license申请id',
    `PRODUCT_CODE` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '产品编码',
    `VERSION`      varchar(12) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '版本号',
    `IP_ADDRESS`   varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'ip地址',
    `CONTAINER`    int(11) NULL DEFAULT NULL COMMENT '容器数',
    `REMARK`       varchar(900) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
    PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = 'license申请产品ip和容器记录表;' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for ppp_product_mediums
-- ----------------------------
DROP TABLE IF EXISTS `ppp_product_mediums`;
CREATE TABLE `ppp_product_mediums`
(
    `ID`                  varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
    `PRODUCT_CODE`        varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '产品代码',
    `MEDIUM_NAME`         varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '介质名',
    `LINK`                varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '下载链接',
    `APPLY_TYPE`          varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '申请方式',
    `LICENSE_DESCRIPTION` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '普元内置许可说明',
    `REMARK`              varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
    `DOC_LINK`            varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '文档地址',
    `CREATED_BY`          varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
    `UPDATED_BY`          varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新人',
    `CREATED_TIME`        datetime(3) NULL DEFAULT NULL COMMENT '创建时间',
    `UPDATED_TIME`        datetime(3) NULL DEFAULT NULL COMMENT '更新时间',
    PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '产品介质表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for ppp_rebate
-- ----------------------------
DROP TABLE IF EXISTS `ppp_rebate`;
CREATE TABLE `ppp_rebate`
(
    `ID`              varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'ID',
    `SALE_UP_LIMIT`   decimal(10, 2) NULL DEFAULT NULL COMMENT '达成销售额上限',
    `SALE_DOWN_LIMIT` decimal(10, 2) NULL DEFAULT NULL COMMENT '达成销售额下限',
    `REBATE_RATE`     decimal(10, 2) NULL DEFAULT NULL COMMENT '返利比例',
    `REBATE_LEVEL`    varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '返利档次',
    PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '返利规则配置表;' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for ppp_rebate_detail
-- ----------------------------
DROP TABLE IF EXISTS `ppp_rebate_detail`;
CREATE TABLE `ppp_rebate_detail`
(
    `ID`                 varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'id',
    `YEAR_TIME`          varchar(4) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '年份(YYYY)',
    `REBATE_TIME`        datetime NULL DEFAULT NULL COMMENT '返利时间',
    `SETTLE_DETAIL_ID`   varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '付款明细ID',
    `ORDER_ID`           varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '订单ID',
    `ORDER_NO`           varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '订单编号',
    `ORDER_AMOUNT`       decimal(10, 2) NULL DEFAULT 0.00 COMMENT '订单金额',
    `REBATE_TYPE`        int(8) NULL DEFAULT NULL COMMENT '返利类型：0:使用；1:新增；',
    `REBATE_AMOUNT`      decimal(10, 2) NULL DEFAULT 0.00 COMMENT '使用返利金额',
    `REMARK`             varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
    `REBATE_RULE_ID`     varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '规则id',
    `REBATE_RATE`        decimal(10, 2) NULL DEFAULT 0.00 COMMENT '返利比率',
    `REBATE_LEVEL`       int(11) NULL DEFAULT NULL COMMENT '返利档次',
    `IS_ACK`             int(11) NULL DEFAULT NULL COMMENT '是否确认；返利类型为新增时有值',
    `CHANNEL_ID`         varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '渠道ID',
    `DELETE_FLAG`        int(2) NULL DEFAULT 0 COMMENT '辑删除标记：0-未删除；1-已删除',
    `CREATED_BY`         varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
    `CREATED_TIME`       datetime NULL DEFAULT NULL COMMENT '创建时间',
    `LAST_MODIFIED_BY`   varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '最后修改人',
    `LAST_MODIFIED_TIME` datetime NULL DEFAULT NULL COMMENT '最后修改时间',
    PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '渠道返利明细表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for ppp_rebate_record
-- ----------------------------
DROP TABLE IF EXISTS `ppp_rebate_record`;
CREATE TABLE `ppp_rebate_record`
(
    `ID`                   varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'id',
    `YEAR_TIME`            varchar(4) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '返利年份(YYYY)',
    `CHANNEL_ID`           varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '渠道信用id',
    `SALE_AMOUNT`          decimal(10, 2) NULL DEFAULT 0.00 COMMENT '销售额',
    `REBATE_AMOUNT`        decimal(10, 2)                                         NOT NULL DEFAULT 0.00 COMMENT '返利金额（=已使用+冻结+未使用）',
    `USED_REBATE_AMOUNT`   decimal(10, 2)                                         NOT NULL DEFAULT 0.00 COMMENT '实际已使用返利金额',
    `FREEZE_REBATE_AMOUNT` decimal(10, 2)                                         NOT NULL DEFAULT 0.00 COMMENT '返利冻结金额（订单付款中的返利）',
    `UNUSED_REBATE_AMOUNT` decimal(10, 2)                                         NOT NULL DEFAULT 0.00 COMMENT '未使用返利金额',
    `REBATE_RULE_ID`       varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '规则id',
    `REBATE_RATE`          decimal(10, 2) NULL DEFAULT 0.00 COMMENT '返利比率',
    `REBATE_LEVEL`         varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '返利档次',
    `CREATED_BY`           varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
    `CREATED_TIME`         datetime NULL DEFAULT NULL COMMENT '创建时间',
    `LAST_MODIFIED_BY`     varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '最后修改人',
    `LAST_MODIFIED_TIME`   datetime NULL DEFAULT NULL COMMENT '最后修改时间',
    PRIMARY KEY (`ID`) USING BTREE,
    UNIQUE INDEX `unique_year_channel`(`YEAR_TIME`, `CHANNEL_ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '渠道返利记录表;' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for ppp_report_info
-- ----------------------------
DROP TABLE IF EXISTS `ppp_report_info`;
CREATE TABLE `ppp_report_info`
(
    `ID`           varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'id',
    `FORUM_ID`     varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '板块id',
    `TARGET_ID`    varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '目标ID',
    `TYPE`         int(11) NULL DEFAULT NULL COMMENT '举报类型（帖子、评论）',
    `STATUS`       int(11) NULL DEFAULT NULL COMMENT '状态',
    `REASON`       varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '举报原因',
    `RESULT`       varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '举报结果',
    `CREATED_BY`   varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建人',
    `CREATED_TIME` datetime(3) NULL DEFAULT NULL COMMENT '创建时间',
    `UPDATED_BY`   varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新人',
    `UPDATED_TIME` datetime(3) NULL DEFAULT NULL COMMENT '更新时间',
    `DELETE_FLAG`  int(11) NULL DEFAULT 0 COMMENT '逻辑删除字段（0/1）',
    PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户举报信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for ppp_request_log
-- ----------------------------
DROP TABLE IF EXISTS `ppp_request_log`;
CREATE TABLE `ppp_request_log`
(
    `ID`                    varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'id',
    `REQUEST_SERIAL_NUMBER` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '流水号',
    `REQUEST_METHOD`        int(11) NULL DEFAULT NULL COMMENT '请求方式  1 - get   2 - post  3-put  4 --delete',
    `REQUEST_HEAD`          varchar(1280) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '请求头',
    `REQUEST_BODY`          varchar(1280) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '请求体',
    `REQUEST_CONSUME_TIME`  int(11) NULL DEFAULT NULL COMMENT '请求耗时(单位毫秒)',
    `REQUEST_TIME`          datetime NULL DEFAULT NULL COMMENT '请求开始时间',
    `REQUEST_SOURCE`        int(11) NULL DEFAULT NULL COMMENT '请求来源  1 -ame  2 - 渠道系统',
    `REQUEST_URL`           varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '请求地址',
    `REQUEST_STATUS`        int(11) NULL DEFAULT NULL COMMENT '请求状态',
    `CREATED_BY`            varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建人',
    `CREATED_TIME`          datetime(3) NULL DEFAULT NULL COMMENT '创建时间',
    `LAST_MODIFIED_BY`      varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新人',
    `LAST_MODIFIED_TIME`    datetime(3) NULL DEFAULT NULL COMMENT '更新时间',
    PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for ppp_resource
-- ----------------------------
DROP TABLE IF EXISTS `ppp_resource`;
CREATE TABLE `ppp_resource`
(
    `ID`                 varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
    `RESOURCE_URL`       varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '资源url',
    `RESOURCE_NAME`      varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '资源名称',
    `RESOURCE_TYPE`      int(11) NULL DEFAULT NULL COMMENT '1-视频 ,2-文档,3-图片 4-首页',
    `RESOURCE_KEY`       varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'oss资源key',
    `RESOURCE_SIZE`      int(44) NULL DEFAULT NULL COMMENT '资源大小(视频时长,文件大小)',
    `VIDEO_SIZE`         int(11) NULL DEFAULT NULL COMMENT '视频时长',
    `HOME_SHOW`          int(11) NULL DEFAULT NULL COMMENT '是否首页展示 0 -不展示,1-展示',
    `TYPE`               int(11) NULL DEFAULT NULL COMMENT '产品方案id',
    `CREATED_BY`         varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建人',
    `CREATED_TIME`       datetime(3) NULL DEFAULT NULL COMMENT '创建时间',
    `LAST_MODIFIED_BY`   varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新人',
    `LAST_MODIFIED_TIME` datetime(3) NULL DEFAULT NULL COMMENT '更新时间',
    PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for ppp_resource_comment
-- ----------------------------
DROP TABLE IF EXISTS `ppp_resource_comment`;
CREATE TABLE `ppp_resource_comment`
(
    `ID`                 varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'ID',
    `TYPE`               int(11) NOT NULL COMMENT '分类(1:产品介绍, 2:解决方案, 3:案例, 4.知识库, 5.文档 99:其他)',
    `RESOURCE_ID`        varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '所属资源id',
    `VISITOR`            varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '评论人名称(非登录用户使用anonymous)',
    `STATUS`             int(11) NULL DEFAULT NULL COMMENT '状态, 0:禁用; 1:启用',
    `CONTENT`            text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '评论内容',
    `CREATED_BY`         varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建人ID',
    `CREATED_TIME`       datetime                                               NOT NULL COMMENT '创建时间',
    `LAST_MODIFIED_BY`   varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '最后修改人ID',
    `LAST_MODIFIED_TIME` datetime                                               NOT NULL COMMENT '最后修改时间',
    PRIMARY KEY (`ID`) USING BTREE,
    INDEX                `PPP_RESOURCE_COMMENT_TYPE`(`TYPE`) USING BTREE,
    INDEX                `PPP_RESOURCE_COMMENT_RESOURCE_ID`(`RESOURCE_ID`) USING BTREE,
    INDEX                `PPP_RESOURCE_COMMENT_STATUS`(`STATUS`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '资源评论' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for ppp_resource_download_record
-- ----------------------------
DROP TABLE IF EXISTS `ppp_resource_download_record`;
CREATE TABLE `ppp_resource_download_record`
(
    `ID`                 varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'ID',
    `TYPE`               int(11) NOT NULL COMMENT '分类(1:产品介绍, 2:解决方案, 3:案例, 4.知识库, 5.文档 99:其他)',
    `RESOURCE_ID`        varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '所属资源id',
    `DOWNLOAD_NAME`      varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '附件名',
    `DOWNLOAD_URL`       varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '附件地址',
    `VISITOR`            varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '下载人名称(非登录用户使用anonymous)',
    `CREATED_BY`         varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建人ID',
    `CREATED_TIME`       datetime                                               NOT NULL COMMENT '创建时间',
    `LAST_MODIFIED_BY`   varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '最后修改人ID',
    `LAST_MODIFIED_TIME` datetime                                               NOT NULL COMMENT '最后修改时间',
    PRIMARY KEY (`ID`) USING BTREE,
    INDEX                `PPP_RESOURCE_DOWNLOAD_RECORD_TYPE`(`TYPE`) USING BTREE,
    INDEX                `PPP_RESOURCE_DOWNLOAD_RECORD_RESOURCE_ID`(`RESOURCE_ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '附件下载记录' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for ppp_resource_history
-- ----------------------------
DROP TABLE IF EXISTS `ppp_resource_history`;
CREATE TABLE `ppp_resource_history`
(
    `ID`                 varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'ID',
    `TYPE`               int(11) NOT NULL COMMENT '分类(1:产品介绍, 2:解决方案, 3:案例, 4.知识库, 5.文档 99:其他)',
    `RESOURCE_ID`        varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '资源id',
    `VISITOR`            varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '浏览人名称(非登录用户使用anonymous)',
    `CREATED_BY`         varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建人ID',
    `CREATED_TIME`       datetime                                               NOT NULL COMMENT '创建时间',
    `LAST_MODIFIED_BY`   varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '最后修改人ID',
    `LAST_MODIFIED_TIME` datetime                                               NOT NULL COMMENT '最后修改时间',
    PRIMARY KEY (`ID`) USING BTREE,
    INDEX                `PPP_RESOURCE_HISTORY_TYPE`(`TYPE`) USING BTREE,
    INDEX                `PPP_RESOURCE_HISTORY_RESOURCE_ID`(`RESOURCE_ID`) USING BTREE,
    INDEX                `PPP_RESOURCE_HISTORY_CREATED_BY`(`CREATED_BY`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '资源浏览历史' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for ppp_resource_search_record
-- ----------------------------
DROP TABLE IF EXISTS `ppp_resource_search_record`;
CREATE TABLE `ppp_resource_search_record`
(
    `ID`                 varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci  NOT NULL COMMENT 'ID',
    `TYPE`               int(11) NOT NULL COMMENT '分类(1:产品介绍, 2:解决方案, 3:案例, 4.知识库, 5.文档 99:其他)',
    `CASE`               int(11) NOT NULL COMMENT '查询分类(1:文字,2:标签)',
    `CONTENT`            varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '检索内容',
    `VISITOR`            varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '检索人名称',
    `CREATED_BY`         varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci  NOT NULL COMMENT '创建人ID',
    `CREATED_TIME`       datetime                                                NOT NULL COMMENT '创建时间',
    `LAST_MODIFIED_BY`   varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci  NOT NULL COMMENT '最后修改人ID',
    `LAST_MODIFIED_TIME` datetime                                                NOT NULL COMMENT '最后修改时间',
    PRIMARY KEY (`ID`) USING BTREE,
    INDEX                `PPP_RESOURCE_SEARCH_RECORD_TYPE`(`TYPE`) USING BTREE,
    INDEX                `PPP_RESOURCE_SEARCH_RECORD_CASE`(`CASE`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户检索记录' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for ppp_role
-- ----------------------------
DROP TABLE IF EXISTS `ppp_role`;
CREATE TABLE `ppp_role`
(
    `ID`                 varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键id',
    `ROLE_NAME`          varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '角色名称',
    `REMARK`             varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
    `STATUS`             int(11) NULL DEFAULT 1 COMMENT '状态：0-禁用；1-启用',
    `CREATED_BY`         varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建人',
    `CREATED_TIME`       datetime(3) NULL DEFAULT NULL COMMENT '创建时间',
    `LAST_MODIFIED_BY`   varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新人',
    `LAST_MODIFIED_TIME` datetime(3) NULL DEFAULT NULL COMMENT '更新时间',
    PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for ppp_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `ppp_role_menu`;
CREATE TABLE `ppp_role_menu`
(
    `ID`                 varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键id',
    `ROLE_ID`            varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '角色ID',
    `MENU_ID`            varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '菜单ID',
    `CREATED_BY`         varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建人',
    `CREATED_TIME`       datetime(3) NULL DEFAULT NULL COMMENT '创建时间',
    `LAST_MODIFIED_BY`   varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新人',
    `LAST_MODIFIED_TIME` datetime(3) NULL DEFAULT NULL COMMENT '更新时间',
    PRIMARY KEY (`ID`) USING BTREE,
    INDEX                `idx_role_id`(`ROLE_ID`) USING BTREE,
    INDEX                `idx_menu_id`(`MENU_ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色与菜单对应关系' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for ppp_settle
-- ----------------------------
DROP TABLE IF EXISTS `ppp_settle`;
CREATE TABLE `ppp_settle`
(
    `ID`                 varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'id',
    `CHANNEL_ID`         varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '渠道ID',
    `SETTLE_NO`          varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '结算付款订单号',
    `ORDERS`             varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '订单号集合',
    `SETTLE_AMOUNT`      decimal(32, 4) NULL DEFAULT 0.0000 COMMENT '本次付款总金额(订单本次之和)=本次使用返利总金额+本次实际付款总金额',
    `REBATE_AMOUNT`      decimal(32, 4) NULL DEFAULT 0.0000 COMMENT '本次使用返利总金额(订单本次之和)',
    `PAY_AMOUNT`         decimal(32, 4) NULL DEFAULT 0.0000 COMMENT '本次实际付款总金额(订单本次之和)',
    `AME_SETTLE_AMOUNT`  decimal(32, 4) NULL DEFAULT 0.0000 COMMENT 'AME+确认付款总金额=AME+确认使用返利金额+AME+确认实际付款金额',
    `AME_REBATE_AMOUNT`  decimal(32, 4) NULL DEFAULT 0.0000 COMMENT 'AME+确认使用返利金额',
    `AME_PAY_AMOUNT`     decimal(32, 4) NULL DEFAULT 0.0000 COMMENT 'AME+确认实际付款金额',
    `SETTLE_STATUS`      int(11) NULL DEFAULT NULL COMMENT '付款状态：1:付款中;2:付款成功;3:付款失败;',
    `SETTLE_TIME`        datetime NULL DEFAULT NULL COMMENT '付款时间',
    `REAL_SETTLE_TIME`   datetime NULL DEFAULT NULL COMMENT '实际付款时间',
    `SETTLE_REMARK`      varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '付款备注',
    `AME_REMARK`         varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'ame+审核备注',
    `CREATED_BY`         varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
    `CREATED_TIME`       datetime NULL DEFAULT NULL COMMENT '创建时间',
    `LAST_MODIFIED_BY`   varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '最后修改人',
    `LAST_MODIFIED_TIME` datetime NULL DEFAULT NULL COMMENT '最后修改时间',
    `IS_LOCK`            int(8) NULL DEFAULT 0 COMMENT '是否锁定：0未锁定，1已锁定',
    `UN_LOCK_TIME`       datetime NULL DEFAULT NULL COMMENT '解锁时间',
    PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '结算付款记录表;' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for ppp_settle_detail
-- ----------------------------
DROP TABLE IF EXISTS `ppp_settle_detail`;
CREATE TABLE `ppp_settle_detail`
(
    `ID`                     varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'ID',
    `SETTLE_ID`              varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '付款记录id',
    `ORDER_ID`               varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '订单ID',
    `ORDER_NO`               varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '订单编号',
    `PAY_TIME`               datetime NULL DEFAULT NULL COMMENT '付款时间',
    `ORDER_AMOUNT`           decimal(32, 4) NULL DEFAULT 0.0000 COMMENT '订单总金额',
    `TAX_ORDER_AMOUNT`       decimal(32, 4) NULL DEFAULT NULL COMMENT '订单总价(含税）',
    `ORDER_PAY_AMOUNT`       decimal(32, 4) NULL DEFAULT 0.0000 COMMENT '订单已付金额(当前订单表数值)',
    `ORDER_SETTLE_AMOUNT`    decimal(32, 4) NULL DEFAULT 0.0000 COMMENT '订单付款总金额(当前订单表数值)',
    `ORDER_REBATE_AMOUNT`    decimal(32, 4) NULL DEFAULT 0.0000 COMMENT '订单使用返利金额(当前订单表数值)',
    `PAY_AMOUNT`             decimal(32, 4) NULL DEFAULT 0.0000 COMMENT '订单付款金额(预计=历史+本次)',
    `REBATE_AMOUNT`          decimal(32, 4) NULL DEFAULT 0.0000 COMMENT '订单使用返利金额(预计=历史+本次)',
    `SETTLE_AMOUNT`          decimal(32, 4) NULL DEFAULT 0.0000 COMMENT '订单付款总金额(预计=历史+本次)=返利+付款',
    `THIS_SETTLE_AMOUNT`     decimal(32, 4) NULL DEFAULT 0.0000 COMMENT '订单付款金额（本次=付款+返利',
    `THIS_PAY_AMOUNT`        decimal(32, 4) NULL DEFAULT 0.0000 COMMENT '订单实际付款金额（本次）',
    `THIS_REBATE_AMOUNT`     decimal(32, 4) NULL DEFAULT 0.0000 COMMENT '订单使用返利金额（本次）',
    `AME_PAY_AMOUNT`         decimal(32, 4) NULL DEFAULT 0.0000 COMMENT 'ame+订单确认已付金额=返利+付款',
    `AME_REBATE_AMOUNT`      decimal(32, 4) NULL DEFAULT 0.0000 COMMENT 'ame+订单确认使用返利金额',
    `AME_SETTLE_AMOUNT`      decimal(32, 4) NULL DEFAULT 0.0000 COMMENT 'ame+订单确认付款总金额',
    `THIS_AME_SETTLE_AMOUNT` decimal(32, 4) NULL DEFAULT 0.0000 COMMENT 'ame+确认订单付款金额（本次）',
    `THIS_AME_PAY_AMOUNT`    decimal(32, 4) NULL DEFAULT 0.0000 COMMENT 'ame+确认订单实际付款金额（本次）',
    `THIS_AME_REBATE_AMOUNT` decimal(32, 4) NULL DEFAULT 0.0000 COMMENT 'ame+确认订单使用返利金额（本次）',
    `PAY_STATUS`             int(8) NULL DEFAULT NULL COMMENT '付款状态：1:付款中;2:付款成功;3:付款失败;',
    `CREATED_BY`             varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
    `CREATED_TIME`           datetime NULL DEFAULT NULL COMMENT '创建时间',
    `LAST_MODIFIED_BY`       varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '最后修改人',
    `LAST_MODIFIED_TIME`     datetime NULL DEFAULT NULL COMMENT '最后修改时间',
    `REAL_PAY_TIME`          datetime NULL DEFAULT NULL COMMENT '实际付款时间',
    PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '结算付款记录订单明细表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for ppp_sys_vars
-- ----------------------------
DROP TABLE IF EXISTS `ppp_sys_vars`;
CREATE TABLE `ppp_sys_vars`
(
    `NAME`               varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci  NOT NULL COMMENT '变量名',
    `VALUE`              varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci  NOT NULL COMMENT '变量值',
    `CNAME`              varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci  NOT NULL COMMENT '变量中文名',
    `DESCRIPTION`        varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '变量描述',
    `CREATED_BY`         varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人ID',
    `CREATED_TIME`       datetime(3) NULL DEFAULT NULL COMMENT '创建时间',
    `LAST_MODIFIED_BY`   varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '最后修改人ID',
    `LAST_MODIFIED_TIME` datetime(3) NULL DEFAULT NULL COMMENT '最后修改时间',
    PRIMARY KEY (`NAME`) USING BTREE,
    INDEX                `PPP_SYS_VARS_CREATED_TIME`(`CREATED_TIME`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = 'PPP系统变量' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for ppp_tag_info
-- ----------------------------
DROP TABLE IF EXISTS `ppp_tag_info`;
CREATE TABLE `ppp_tag_info`
(
    `ID`                 varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'ID',
    `NAME`               varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '名称',
    `TYPE`               varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '分类(字典定义)',
    `DESCRIPTION`        varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '描述',
    `STATUS`             int(11) NOT NULL COMMENT '状态, 0:禁用; 1:启用',
    `CREATED_BY`         varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人ID',
    `CREATED_TIME`       datetime NULL DEFAULT NULL COMMENT '创建时间',
    `LAST_MODIFIED_BY`   varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '最后修改人ID',
    `LAST_MODIFIED_TIME` datetime NULL DEFAULT NULL COMMENT '最后修改时间',
    PRIMARY KEY (`ID`) USING BTREE,
    INDEX                `PPP_TAG_INFO_ID`(`ID`) USING BTREE,
    INDEX                `PPP_TAG_INFO_NAME`(`NAME`) USING BTREE,
    INDEX                `PPP_TAG_INFO_TYPE`(`STATUS`) USING BTREE,
    INDEX                `PPP_TAG_INFO_STATUS`(`STATUS`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '标签信息' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for ppp_tag_resource
-- ----------------------------
DROP TABLE IF EXISTS `ppp_tag_resource`;
CREATE TABLE `ppp_tag_resource`
(
    `ID`                 varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'ID',
    `TYPE`               int(11) NOT NULL COMMENT '分类(1:产品介绍, 2:解决方案, 3:案例, 4.知识库, 5.文档 99:其他)',
    `TAG_ID`             varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '标签ID',
    `RESOURCE_ID`        varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '资源ID',
    `CREATED_BY`         varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人ID',
    `CREATED_TIME`       datetime NULL DEFAULT NULL COMMENT '创建时间',
    `LAST_MODIFIED_BY`   varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '最后修改人ID',
    `LAST_MODIFIED_TIME` datetime NULL DEFAULT NULL COMMENT '最后修改时间',
    PRIMARY KEY (`ID`) USING BTREE,
    INDEX                `PPP_TAG_RESOURCE_ID`(`ID`) USING BTREE,
    INDEX                `PPP_TAG_RESOURCE_TYPE`(`TYPE`) USING BTREE,
    INDEX                `PPP_TAG_RESOURCE_CATALOG_ID`(`TAG_ID`) USING BTREE,
    INDEX                `PPP_TAG_RESOURCE_RESOURCE_ID`(`RESOURCE_ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '标签资源' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for ppp_target_customer
-- ----------------------------
DROP TABLE IF EXISTS `ppp_target_customer`;
CREATE TABLE `ppp_target_customer`
(
    `ID`          varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'id',
    `CUSTOM_NAME` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '客户名称',
    `CUSTOM_CODE` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '客户信用代码',
    PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '目标客户表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for ppp_theme
-- ----------------------------
DROP TABLE IF EXISTS `ppp_theme`;
CREATE TABLE `ppp_theme`
(
    `ID`                 varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键id',
    `THEME_NAME`         varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '题目名称',
    `THEME_TYPE`         int(11) NULL DEFAULT NULL COMMENT '题目类型：1-单选题；2-多选题；3-是非题；4-填空题',
    `TYPE`               varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '产品方案分类',
    `IS_MUST`            int(11) NULL DEFAULT 0 COMMENT '是否必选：0-否；1-是',
    `THEME_ANSWER`       varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '题目答案',
    `THEME_WEIGHT`       int(11) NULL DEFAULT 0 COMMENT '题目权重（排序用）',
    `STATUS`             int(11) NULL DEFAULT 1 COMMENT '状态：0-停用；1-启用',
    `CREATED_BY`         varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建人',
    `CREATED_TIME`       datetime(3) NULL DEFAULT NULL COMMENT '创建时间',
    `LAST_MODIFIED_BY`   varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新人',
    `LAST_MODIFIED_TIME` datetime(3) NULL DEFAULT NULL COMMENT '更新时间',
    PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '考试题目' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for ppp_theme_option
-- ----------------------------
DROP TABLE IF EXISTS `ppp_theme_option`;
CREATE TABLE `ppp_theme_option`
(
    `ID`                 varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键id',
    `THEME_ID`           varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '考试题目id',
    `OPTION_CODE`        varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '选项代码：A/B/C/D等',
    `OPTION_DESC`        varchar(1024) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '选项描述',
    `IS_ANSWER`          int(11) NULL DEFAULT 0 COMMENT '是否正确答案：0-否；1-是',
    `CREATED_BY`         varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建人',
    `CREATED_TIME`       datetime(3) NULL DEFAULT NULL COMMENT '创建时间',
    `LAST_MODIFIED_BY`   varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新人',
    `LAST_MODIFIED_TIME` datetime(3) NULL DEFAULT NULL COMMENT '更新时间',
    PRIMARY KEY (`ID`) USING BTREE,
    INDEX                `idx_theme_id`(`THEME_ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '题目选项' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for ppp_user
-- ----------------------------
DROP TABLE IF EXISTS `ppp_user`;
CREATE TABLE `ppp_user`
(
    `ID`           varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键：用户id',
    `USERNAME`     varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '用户名',
    `NICKNAME`     varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '昵称',
    `REALNAME`     varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '真实名称',
    `MOBILE`       varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '手机号码',
    `EMAIL`        varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '邮箱',
    `ID_CARD`      varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '身份证号',
    `HEADER`       varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '头像',
    `ROLE`         int(11) NULL DEFAULT 1 COMMENT '角色：1-普通用户；2-企业用户；3-企业管理员；4-渠道用户；5-渠道销售',
    `COMPANY_ID`   varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '所属公司id',
    `COMPANY_NAME` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '公司名称',
    `DEPARTMENT`   varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '部门',
    `POSITION`     varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '职位',
    `INDUSTRY`     varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '所属行业',
    `BINDING_TIME` datetime(3) NULL DEFAULT NULL COMMENT '绑定公司时间',
    `IS_INTERNAL`  int(11) NULL DEFAULT 0 COMMENT '是否内部员工：0-否；1-是',
    `STATUS`       int(11) NULL DEFAULT 1 COMMENT '状态：0-停用；1-启用',
    `CREATED_TIME` datetime(3) NULL DEFAULT NULL COMMENT '注册时间',
    `UPDATED_TIME` datetime(3) NULL DEFAULT NULL COMMENT '更新时间',
    PRIMARY KEY (`ID`) USING BTREE,
    INDEX          `idx_company_id`(`COMPANY_ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '门户用户' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for ppp_user_account
-- ----------------------------
DROP TABLE IF EXISTS `ppp_user_account`;
CREATE TABLE `ppp_user_account`
(
    `ID`               varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
    `USER_ID`          varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户id',
    `ACCOUNT_TYPE`     int(11) NOT NULL COMMENT '账户类型：1-用户账号；2-手机号；3-阿米加',
    `AUTH_ACCOUNT`     varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '授权账号：可以是用户名、手机号、第三方账号等',
    `AUTH_CERTIFICATE` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '授权凭证：密码或其他',
    `STATUS`           int(11) NULL DEFAULT 1 COMMENT '状态：0-停用；1-启用',
    `CREATED_TIME`     datetime(3) NULL DEFAULT NULL COMMENT '创建时间',
    `UPDATED_TIME`     datetime(3) NULL DEFAULT NULL COMMENT '更新时间',
    PRIMARY KEY (`ID`) USING BTREE,
    INDEX              `idx_user_id`(`USER_ID`) USING BTREE,
    INDEX              `idx_auth_account`(`AUTH_ACCOUNT`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户账户' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for ppp_user_action_config
-- ----------------------------
DROP TABLE IF EXISTS `ppp_user_action_config`;
CREATE TABLE `ppp_user_action_config`
(
    `ID`                 varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键id',
    `ACTION_TYPE`        int(11) NOT NULL COMMENT '行为类型：1-浏览；2-下载；3-搜索；4-分享；5-登录',
    `RECORD_SCOPE`       varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '记录作用域：0-全部；1-产品介绍；2-解决方案；3-案例；4-知识库；5-文档; 33-客户',
    `STATUS`             int(11) NULL DEFAULT 0 COMMENT '状态：0-关闭；1-开启',
    `CREATED_BY`         varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建人',
    `CREATED_TIME`       datetime(3) NULL DEFAULT NULL COMMENT '创建时间',
    `LAST_MODIFIED_BY`   varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新人',
    `LAST_MODIFIED_TIME` datetime(3) NULL DEFAULT NULL COMMENT '更新时间',
    PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户行为记录配置' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for ppp_user_auth
-- ----------------------------
DROP TABLE IF EXISTS `ppp_user_auth`;
CREATE TABLE `ppp_user_auth`
(
    `ID`           varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键id',
    `USER_ID`      varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '用户id',
    `AUTH_ID`      varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '认证项目id',
    `AUTH_STATUS`  int(11) NULL DEFAULT 1 COMMENT '认证状态：1-培训中；2-培训完成；3-认证通过；4-认证未通过',
    `EXAM_STATUS`  int(11) NULL DEFAULT 1 COMMENT '考试状态：1-未考试；2-考试通过；3-考试未通过',
    `EXAM_SCORE`   int(11) NULL DEFAULT NULL COMMENT '考试成绩（最高分）',
    `EXAM_COUNT`   int(11) NULL DEFAULT 0 COMMENT '考试次数',
    `CCIE_NO`      varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '证书编号',
    `AWARD_TIME`   datetime(3) NULL DEFAULT NULL COMMENT '颁证时间',
    `INDATE`       int(11) NULL DEFAULT NULL COMMENT '有效期（月）',
    `CREATED_TIME` datetime(3) NULL DEFAULT NULL COMMENT '创建时间',
    `UPDATED_TIME` datetime(3) NULL DEFAULT NULL COMMENT '更新时间',
    PRIMARY KEY (`ID`) USING BTREE,
    INDEX          `idx_user_auth_id`(`USER_ID`, `AUTH_ID`) USING BTREE,
    INDEX          `idx_auth_id`(`AUTH_ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户认证记录' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for ppp_user_auth_course
-- ----------------------------
DROP TABLE IF EXISTS `ppp_user_auth_course`;
CREATE TABLE `ppp_user_auth_course`
(
    `ID`           varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键id',
    `USER_ID`      varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '用户id',
    `COURSE_ID`    varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '课程id',
    `PROGRESS`     decimal(5, 2) NULL DEFAULT 0.00 COMMENT '学习进度（百分比）',
    `IS_UNLOCKED`  int(11) NULL DEFAULT 1 COMMENT '是否解锁：0-否；1-是',
    `STUDY_STATUS` int(11) NULL DEFAULT 1 COMMENT '课程学习状态：1-待学习；2-学习中；3-已学习',
    `CREATED_TIME` datetime(3) NULL DEFAULT NULL COMMENT '创建时间',
    `UPDATED_TIME` datetime(3) NULL DEFAULT NULL COMMENT '更新时间',
    PRIMARY KEY (`ID`) USING BTREE,
    INDEX          `idx_record_id`(`USER_ID`) USING BTREE,
    INDEX          `idx_course_id`(`COURSE_ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户认证-课程学习记录' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for ppp_user_auth_exam
-- ----------------------------
DROP TABLE IF EXISTS `ppp_user_auth_exam`;
CREATE TABLE `ppp_user_auth_exam`
(
    `ID`            varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键id',
    `RECORD_ID`     varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '认证记录id',
    `PAPER_ID`      varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '试卷id',
    `START_TIME`    datetime(3) NULL DEFAULT NULL COMMENT '考试开始时间',
    `END_TIME`      datetime(3) NULL DEFAULT NULL COMMENT '考试结束时间',
    `EXAM_SCORE`    int(11) NULL DEFAULT NULL COMMENT '考试成绩',
    `USE_TIME`      int(255) NULL DEFAULT NULL COMMENT '用时（秒）',
    `EXAM_STATUS`   int(11) NULL DEFAULT 1 COMMENT '考试状态：1-考试中；2-考试通过；3-考试未通过',
    `EXAM_CONTENT`  text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '考卷内容（包含我的答案）',
    `PAPER_CONTENT` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '试卷内容（包含正确答案）',
    `CREATED_TIME`  datetime(3) NULL DEFAULT NULL COMMENT '创建时间',
    `UPDATED_TIME`  datetime(3) NULL DEFAULT NULL COMMENT '更新时间',
    PRIMARY KEY (`ID`) USING BTREE,
    INDEX           `idx_record_id`(`RECORD_ID`) USING BTREE,
    INDEX           `idx_paper_id`(`PAPER_ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户认证-考试记录' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for ppp_user_binding_apply
-- ----------------------------
DROP TABLE IF EXISTS `ppp_user_binding_apply`;
CREATE TABLE `ppp_user_binding_apply`
(
    `ID`           varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
    `USER_ID`      varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '申请用户id',
    `REALNAME`     varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '用户姓名',
    `DEPARTMENT`   varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '用户所属部门',
    `COMPANY_ID`   varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '申请绑定的企业id',
    `APPLY_TIME`   datetime(3) NULL DEFAULT NULL COMMENT '申请时间',
    `AUDIT_STATUS` int(11) NULL DEFAULT 1 COMMENT '企业审核状态：1-待审核；2-通过；3-忽略',
    `AUDIT_BY`     varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '审核人',
    `AUDIT_TIME`   datetime(3) NULL DEFAULT NULL COMMENT '审核时间',
    PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户绑定公司申请记录' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for ppp_user_browse_record
-- ----------------------------
DROP TABLE IF EXISTS `ppp_user_browse_record`;
CREATE TABLE `ppp_user_browse_record`
(
    `ID`              varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键id',
    `USER_ID`         varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户id',
    `HAPPEN_TIME`     datetime(3) NULL DEFAULT NULL COMMENT '发生时间',
    `TARGET_ID`       varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '目标id',
    `TARGET_TYPE`     int(11) NULL DEFAULT NULL COMMENT '目标类型：1-产品介绍；2-解决方案；3-案例；4-知识库；5-文档; 33-客户; 99-其他',
    `TARGET_NAME`     varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '目标名称',
    `TARGET_SUMMARY`  varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '目标摘要信息',
    `BROWSE_DURATION` decimal(5, 2) NULL DEFAULT NULL COMMENT '浏览时长',
    PRIMARY KEY (`ID`) USING BTREE,
    INDEX             `idx_user_id`(`USER_ID`) USING BTREE,
    INDEX             `idx_target_id`(`TARGET_ID`) USING BTREE,
    INDEX             `idx_happen_time`(`HAPPEN_TIME`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户浏览记录' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for ppp_user_download_record
-- ----------------------------
DROP TABLE IF EXISTS `ppp_user_download_record`;
CREATE TABLE `ppp_user_download_record`
(
    `ID`              varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键id',
    `USER_ID`         varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户id',
    `HAPPEN_TIME`     datetime(3) NULL DEFAULT NULL COMMENT '发生时间',
    `TARGET_ID`       varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '目标id',
    `TARGET_TYPE`     int(11) NULL DEFAULT NULL COMMENT '目标类型：1-产品介绍；2-解决方案；3-案例；4-知识库；5-文档; 33-客户; 99-其他',
    `ATTACHMENT_NAME` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '附件名称',
    `ATTACHMENT_URL`  varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '附件地址',
    PRIMARY KEY (`ID`) USING BTREE,
    INDEX             `idx_user_id`(`USER_ID`) USING BTREE,
    INDEX             `idx_target_id`(`TARGET_ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户下载记录' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for ppp_user_favorite
-- ----------------------------
DROP TABLE IF EXISTS `ppp_user_favorite`;
CREATE TABLE `ppp_user_favorite`
(
    `ID`             varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'ID',
    `USER_ID`        varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户ID',
    `TARGET_ID`      varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '目标ID',
    `TARGET_TYPE`    int(11) NULL DEFAULT NULL COMMENT '目标类型, 1:产品介绍; 2:解决方案; 3:案例; 4:知识库',
    `TARGET_NAME`    varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '目标名称',
    `TARGET_SUMMARY` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '目标摘要信息',
    `CREATED_TIME`   datetime(3) NULL DEFAULT NULL COMMENT '创建时间',
    PRIMARY KEY (`ID`) USING BTREE,
    INDEX            `PPP_USER_FAVORITE_USER_ID`(`USER_ID`) USING BTREE,
    INDEX            `PPP_USER_FAVORITE_TARGET_ID`(`TARGET_ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户收藏' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for ppp_user_login_record
-- ----------------------------
DROP TABLE IF EXISTS `ppp_user_login_record`;
CREATE TABLE `ppp_user_login_record`
(
    `ID`                  varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键id',
    `USER_ID`             varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户id',
    `HAPPEN_TIME`         datetime(3) NULL DEFAULT NULL COMMENT '发生时间',
    `IP_ADDRESS`          varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT 'ip地址',
    `GEOGRAPHIC_LOCATION` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '地理位置',
    `LOGIN_TYPE`          int(11) NULL DEFAULT NULL COMMENT '登录方式：1-账号密码；2-手机验证码；3-阿米加',
    `LOGIN_DEVICE`        varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '登录设备',
    PRIMARY KEY (`ID`) USING BTREE,
    INDEX                 `idx_user_id`(`USER_ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户登录记录' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for ppp_user_message
-- ----------------------------
DROP TABLE IF EXISTS `ppp_user_message`;
CREATE TABLE `ppp_user_message`
(
    `ID`          varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键id',
    `USER_ID`     varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户id',
    `MSG_ID`      varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '消息id',
    `TITLE`       varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '消息标题',
    `SUMMARY`     varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '消息摘要',
    `PUSH_TIME`   datetime(3) NULL DEFAULT NULL COMMENT '消息推送时间',
    `IS_READ`     int(11) NULL DEFAULT 0 COMMENT '是否阅读：0-未读；1-已读',
    `READ_TIME`   datetime(3) NULL DEFAULT NULL COMMENT '阅读时间',
    `DELETE_FLAG` int(11) NULL DEFAULT 0 COMMENT '逻辑删除标记：0-未删除；1-已删除',
    PRIMARY KEY (`ID`) USING BTREE,
    INDEX         `idx_user_id`(`USER_ID`) USING BTREE,
    INDEX         `idx_msg_id`(`MSG_ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户消息' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for ppp_user_role
-- ----------------------------
DROP TABLE IF EXISTS `ppp_user_role`;
CREATE TABLE `ppp_user_role`
(
    `ID`                 varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键id',
    `USER_ID`            varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户ID',
    `ROLE_ID`            varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '角色ID',
    `CREATED_BY`         varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建人',
    `CREATED_TIME`       datetime(3) NULL DEFAULT NULL COMMENT '创建时间',
    `LAST_MODIFIED_BY`   varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新人',
    `LAST_MODIFIED_TIME` datetime(3) NULL DEFAULT NULL COMMENT '更新时间',
    PRIMARY KEY (`ID`) USING BTREE,
    INDEX                `idx_user_id`(`USER_ID`) USING BTREE,
    INDEX                `idx_role_id`(`ROLE_ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户与角色对应关系' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for ppp_user_search_record
-- ----------------------------
DROP TABLE IF EXISTS `ppp_user_search_record`;
CREATE TABLE `ppp_user_search_record`
(
    `ID`             varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键id',
    `USER_ID`        varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户id',
    `HAPPEN_TIME`    datetime(3) NULL DEFAULT NULL COMMENT '发生时间',
    `TARGET_TYPE`    int(11) NULL DEFAULT NULL COMMENT '目标类型：1-产品介绍；2-解决方案；3-案例；4-知识库；5-文档; 33-客户; 99-其他',
    `SEARCH_TYPE`    int(11) NULL DEFAULT NULL COMMENT '搜索方式：1-关键字；2-分类；3-标签',
    `SEARCH_CONTENT` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '搜索内容',
    PRIMARY KEY (`ID`) USING BTREE,
    INDEX            `idx_user_id`(`USER_ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户搜索记录' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for ppp_user_share_record
-- ----------------------------
DROP TABLE IF EXISTS `ppp_user_share_record`;
CREATE TABLE `ppp_user_share_record`
(
    `ID`             varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键id',
    `USER_ID`        varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户id',
    `HAPPEN_TIME`    datetime(3) NULL DEFAULT NULL COMMENT '发生时间',
    `TARGET_ID`      varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '目标id',
    `TARGET_TYPE`    int(11) NULL DEFAULT NULL COMMENT '目标类型：1-产品介绍；2-解决方案；3-案例；4-知识库；5-文档; 33-客户; 99-其他',
    `TARGET_NAME`    varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '目标名称',
    `TARGET_SUMMARY` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '目标摘要信息',
    `SHARE_CHANNEL`  int(11) NULL DEFAULT NULL COMMENT '分享渠道：1-微信',
    PRIMARY KEY (`ID`) USING BTREE,
    INDEX            `idx_user_id`(`USER_ID`) USING BTREE,
    INDEX            `idx_target_id`(`TARGET_ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户分享记录' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for ppp_user_votes
-- ----------------------------
DROP TABLE IF EXISTS `ppp_user_votes`;
CREATE TABLE `ppp_user_votes`
(
    `ID`           varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'id',
    `USER_ID`      varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户id',
    `VOTE_ID`      varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '投票id',
    `OPTIONS`      varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '投票项id列表',
    `CREATED_BY`   varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建人',
    `CREATED_TIME` datetime(3) NULL DEFAULT NULL COMMENT '创建时间',
    `DELETE_FLAG`  int(11) NULL DEFAULT 0 COMMENT '逻辑删除字段（0/1）',
    PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户投票信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for ppp_vote
-- ----------------------------
DROP TABLE IF EXISTS `ppp_vote`;
CREATE TABLE `ppp_vote`
(
    `ID`           varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'id',
    `IS_PUBLIC`    int(11) NULL DEFAULT 0 COMMENT '是否公开',
    `MAX_OPTION`   int(11) NULL DEFAULT NULL COMMENT '最多选项',
    `TITLE`        varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标题',
    `TOTAL_VOTES`  int(11) NULL DEFAULT 0 COMMENT '总票数',
    `VOTES_PER`    int(11) NULL DEFAULT 0 COMMENT '人投票人数',
    `DEAD_LINE`    datetime(3) NULL DEFAULT NULL COMMENT '投票截止日期',
    `CREATED_BY`   varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建人',
    `CREATED_TIME` datetime(3) NULL DEFAULT NULL COMMENT '创建时间',
    `DELETE_FLAG`  int(11) NULL DEFAULT 0 COMMENT '逻辑删除字段（0/1）',
    PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '投票表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for ppp_vote_option
-- ----------------------------
DROP TABLE IF EXISTS `ppp_vote_option`;
CREATE TABLE `ppp_vote_option`
(
    `ID`          varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'id',
    `VOTE_ID`     varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '投票ID',
    `OPTION_NAME` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '选项名',
    `VOTES`       int(11) NULL DEFAULT 0 COMMENT '票数',
    `DELETE_FLAG` int(11) NULL DEFAULT 0 COMMENT '逻辑删除字段（0/1）',
    PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '投票项' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for temp_know
-- ----------------------------
DROP TABLE IF EXISTS `temp_know`;
CREATE TABLE `temp_know`
(
    `ID`               varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键id',
    `TITLE`            varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '标题',
    `BODY`             mediumtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '内容',
    `AUTHOR`           varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '作者',
    `TYPE`             varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '分类',
    `CREATED_BY`       varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建人',
    `CREATIONDATE`     datetime(3) NULL DEFAULT NULL COMMENT '创建时间',
    `LAST_MODIFIED_BY` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新人',
    `LASTMODDATE`      datetime(3) NULL DEFAULT NULL COMMENT '更新时间',
    PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = 'doc知识库临时表' ROW_FORMAT = DYNAMIC;

SET
FOREIGN_KEY_CHECKS = 1;
